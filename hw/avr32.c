/*
 * AVR32 ATSTK1000 System emulation.
 *
 * Copyright (c) 2009 Rabin Vincent
 *
 * This code is licenced under the GNU GPLv2.
 */

/* FIXME split this into separate files for AVR32 core code and ATSTK1000 */

#include "cpu.h"
#include "sysbus.h"
#include "devices.h"
#include "sysemu.h"
#include "boards.h"
#include "net.h"
#include "qemu-char.h"
#include "qemu-timer.h"
#include "devices.h"
#include "loader.h"
#include "elf.h"

static uint32_t bootstrap_pc;

#ifdef DEBUG
#define dbg_read(base) fprintf(stderr, "%s: 0x%08x\n", __func__, (uint32_t)base + offset);
#define dbg_write(base) fprintf(stderr, "%s: 0x%08x = 0x%08x\n", __func__, (uint32_t)base + offset, value);
#define info_read(base) fprintf(stderr, "%s: 0x%08x\n", __func__, (uint32_t)base + offset);
#define info_write(base) fprintf(stderr, "%s: 0x%08x = 0x%08x\n", __func__, (uint32_t)base + offset, value);
#else
#define dbg_read(base)
#define dbg_write(base)
#define info_read(base)
#define info_write(base)
#endif

#define AVR32_PIC_CPU_INT3 0
#define AVR32_PIC_CPU_INT2 1
#define AVR32_PIC_CPU_INT1 2
#define AVR32_PIC_CPU_INT0 3
#define AVR32_PIC_CPU_NMI  4

static void avr32_pic_cpu_handler(void *opaque, int irq, int level)
{
    CPUState *env = (CPUState *)opaque;
    int type;

    switch (irq) {
    case AVR32_PIC_CPU_INT3 ... AVR32_PIC_CPU_INT0:
        /* FIXME ? */
        type = CPU_INTERRUPT_HARD;
        break;

    case AVR32_PIC_CPU_NMI:
        type = CPU_INTERRUPT_NMI;
        break;

    default:
        cpu_abort(env, "%s: unknown irq type\n", __func__);
    }

    if (level)
        cpu_interrupt(env, type);
    else
        cpu_reset_interrupt(env, type);
}

static qemu_irq *avr32_pic_init_cpu(CPUState *env)
{
    return qemu_allocate_irqs(avr32_pic_cpu_handler, env, 5);
}

#define INTC_IPR0   0
#define INTC_IPR63  256
#define INTC_IRR0   260
#define INTC_IRR63  508
#define INTC_ICR3   512
#define INTC_ICR2   516
#define INTC_ICR1   520
#define INTC_ICR0   524

typedef struct at32ap_intc_state {
    uint32_t page_adjust;
    qemu_irq parent_irq[5];
    CPUState *env;

    uint64_t pending;

    uint32_t ipr[64];
    uint32_t irr[64];
    uint32_t icr[4];
} at32ap_intc_state;

static uint32_t at32ap_intc_read(void *opaque, target_phys_addr_t offset)
{
    at32ap_intc_state *s = opaque;

    offset -= s->page_adjust;

    // dbg_read(0xff000400);

    switch (offset) {
        case INTC_IPR0 ... INTC_IPR63:
            return s->ipr[(offset - INTC_IPR0) >> 2];

        case INTC_IRR0 ... INTC_IRR63:
            return s->irr[(offset - INTC_IRR0) >> 2];

        case INTC_ICR3 ... INTC_ICR0:
            return s->icr[3 - ((offset - INTC_ICR3) >> 2)];
    }

    return 0;
}

static void at32ap_intc_write(void *opaque, target_phys_addr_t offset,
                             uint32_t value)
{
    at32ap_intc_state *s = opaque;

    offset -= s->page_adjust;

    // dbg_write(0xff000400);

    switch (offset) {
        case INTC_IPR0 ... INTC_IPR63:
            s->ipr[(offset - INTC_IPR0) >> 2] = value;
            break;

        case INTC_IRR0 ... INTC_IRR63:
            s->irr[(offset - INTC_IRR0) >> 2] = value;
            break;
    }
}

static CPUReadMemoryFunc *at32ap_intc_readfn[] = {
   at32ap_intc_read,
   at32ap_intc_read,
   at32ap_intc_read
};

static CPUWriteMemoryFunc *at32ap_intc_writefn[] = {
   at32ap_intc_write,
   at32ap_intc_write,
   at32ap_intc_write,
};

static void at32ap_intc_update(at32ap_intc_state *s)
{
    if (s->pending) {
        int group = ffsll(s->pending) - 1;

        /* FIXME handle other INT levels */
        s->env->autovector[0] = s->ipr[group] & 0x3fff;
        s->icr[0] = group;
    }

    qemu_set_irq(s->parent_irq[AVR32_PIC_CPU_INT0], !!s->pending);
}

static void at32ap_intc_set_irq(void *opaque, int irq, int level)
{
    at32ap_intc_state *s = opaque;
    int group = irq / 32;
    int line = irq % 32;

    if (level) {
        s->pending |= 1 << group;
        s->irr[group] |= 1 << line;
    } else {
        s->pending &= ~(1 << group);
        s->irr[group] &= ~(1 << line);
    }

    at32ap_intc_update(s);
}

static qemu_irq *intc_init(uint32_t base, CPUState *env, qemu_irq *parent_irqs)
{
    at32ap_intc_state *s;
    qemu_irq *qi;
    int iomemtype;

    s = qemu_mallocz(sizeof(at32ap_intc_state));
    qi = qemu_allocate_irqs(at32ap_intc_set_irq, s, 2048);

    s->env = env;

    s->parent_irq[AVR32_PIC_CPU_INT0] = parent_irqs[AVR32_PIC_CPU_INT0];
    s->parent_irq[AVR32_PIC_CPU_INT1] = parent_irqs[AVR32_PIC_CPU_INT1];
    s->parent_irq[AVR32_PIC_CPU_INT2] = parent_irqs[AVR32_PIC_CPU_INT2];
    s->parent_irq[AVR32_PIC_CPU_INT3] = parent_irqs[AVR32_PIC_CPU_INT3];
    s->parent_irq[AVR32_PIC_CPU_NMI] = parent_irqs[AVR32_PIC_CPU_NMI];

    s->page_adjust = base & (TARGET_PAGE_SIZE - 1);
    iomemtype = cpu_register_io_memory(at32ap_intc_readfn,
                                       at32ap_intc_writefn, s);
    cpu_register_physical_memory(base, 0x400, iomemtype);

    return qi;
}


typedef struct at91_usart_state {
    uint32_t page_adjust;

    uint8_t rhr;
    uint32_t imr;
    uint32_t csr;

    int status;

    int recvd;
    qemu_irq irq;

    CharDriverState *chr;
} at91_usart_state;

#define US_CR   0x00
#define US_MR   0x04
#define US_IER  0x08
#define US_IDR  0x0C
#define US_IMR  0x10
#define US_CSR  0x14
#define US_RHR  0x18
#define US_THR  0x1C

#define US_CR_RSTSTA   (1 << 8)

#define US_RXRDY    (1 << 0)
#define US_TXRDY    (1 << 1)
#define US_RXBRK    (1 << 2)
#define US_TXEMPTY  (1 << 9)

static void at91_usart_irq(at91_usart_state *s)
{
    uint32_t new_status = !!(s->imr & s->csr);

    if (new_status != s->status) {
        s->status = new_status;
        qemu_set_irq(s->irq, s->status);
    }
}

static uint32_t at91_usart_read(void *opaque, target_phys_addr_t offset)
{
    at91_usart_state *s = opaque;
    uint32_t temp;

    offset -= s->page_adjust;

    if (offset != US_CSR)
        dbg_read(0xffe01000);

    switch (offset) {
        case US_IMR:
            return s->imr;

        case US_CSR:
            return s->csr;

        case US_RHR:
            temp = s->rhr;
            s->rhr = 0;
            s->csr &= ~(US_RXRDY | US_RXBRK);
            at91_usart_irq(s);
            s->recvd = 0;
            return temp;
    }

    return 0;
}

static void at91_usart_write(void *opaque, target_phys_addr_t offset,
                             uint32_t value)
{
    at91_usart_state *s = opaque;
    uint8_t ch;

    offset -= s->page_adjust;

    if (offset != US_THR)
        dbg_write(0xffe01000);

    switch (offset) {
        case US_CR:
            if (value & US_CR_RSTSTA)
                s->csr &= ~US_RXBRK;
            break;

        case US_IER:
            s->imr |= value;
            at91_usart_irq(s);
            break;

        case US_IDR:
            s->imr &= ~value;
            at91_usart_irq(s);
            break;

        case US_THR:
            ch = value;
            if (s->chr)
                qemu_chr_write(s->chr, &ch, 1);

            s->csr |= US_TXRDY | US_TXEMPTY;
            at91_usart_irq(s);
            break;
    }
}

static CPUReadMemoryFunc *at91_usart_readfn[] = {
   at91_usart_read,
   at91_usart_read,
   at91_usart_read
};

static CPUWriteMemoryFunc *at91_usart_writefn[] = {
   at91_usart_write,
   at91_usart_write,
   at91_usart_write
};

static int at91_usart_can_receive(void *opaque)
{
    at91_usart_state *s = opaque;

    return !s->recvd;
}

static void at91_usart_receive(void *opaque, const uint8_t *buf, int size)
{
    at91_usart_state *s = opaque;

    if (size > 1) {
        printf("%s dropped %d chars\n", __func__, size - 1);
    }

    s->rhr = *buf;
    s->recvd = 1;
    s->csr |= US_RXRDY;
    at91_usart_irq(s);
}

static void at91_usart_event(void *opaque, int event)
{
    at91_usart_state *s = opaque;

    if (event == CHR_EVENT_BREAK) {
        s->csr |= US_RXBRK;
        at91_usart_irq(s);
    }
}

static void at91_usart_init(uint32_t base, qemu_irq irq, CharDriverState *chr)
{
    at91_usart_state *s;
    int iomemtype;

    s = qemu_mallocz(sizeof(at91_usart_state));
    s->irq = irq;
    s->chr = chr;
    s->csr = US_TXRDY | US_TXEMPTY;

    if (chr)
        qemu_chr_add_handlers(chr, at91_usart_can_receive,
                              at91_usart_receive, at91_usart_event, s);


    s->page_adjust = base & (TARGET_PAGE_SIZE - 1);
    iomemtype = cpu_register_io_memory(at91_usart_readfn,
                                       at91_usart_writefn, s);
    cpu_register_physical_memory(base, 0x200, iomemtype);
}

struct AVR32Counter {
    QEMUTimer *timer;
    qemu_irq irq;

    int64_t base;
    uint32_t freq;
};

static void avr32_counter_tick(void *opaque)
{
    CPUState *env = opaque;
    AVR32Counter *counter = env->counter;

    qemu_set_irq(counter->irq, 1);
}

uint32_t avr32_read_counter(void)
{
    AVR32Counter *counter = cpu_single_env->counter;
    int64_t now = qemu_get_clock(vm_clock) - counter->base;

    return muldiv64(now, counter->freq, get_ticks_per_sec());
}

void avr32_write_compare(uint32_t value)
{
    AVR32Counter *counter = cpu_single_env->counter;
    uint64_t next;

    if (!value) {
        qemu_set_irq(counter->irq, 0);
        qemu_del_timer(counter->timer);
        return;
    }

    next = muldiv64(value, get_ticks_per_sec(), counter->freq);
    qemu_mod_timer(counter->timer, next);
}

static void atstk_init(ram_addr_t ram_size,
                     const char *boot_device,
                     const char *kernel_filename, const char *kernel_cmdline,
                     const char *initrd_filename, const char *cpu_model)
{
    CPUState *env;
    ram_addr_t sram_offset;
    ram_addr_t ram_offset;
    ram_addr_t sram_size;
    qemu_irq *cpu_pic;
    qemu_irq *pic;
    AVR32Counter *counter;

    if (!cpu_model)
        cpu_model = "arm926";
    env = cpu_init(cpu_model);
    if (!env) {
        fprintf(stderr, "Unable to find CPU definition\n");
        exit(1);
    }

    sram_size = 10 * 1024 * 1024;

    sram_offset = qemu_ram_alloc(sram_size);
    cpu_register_physical_memory(0, sram_size, sram_offset | IO_MEM_RAM);

    cpu_register_physical_memory(0x24000000, 0x8000,
				 qemu_ram_alloc(0x8000) | IO_MEM_RAM);

    ram_size = 64 * 1024 * 1024;
    ram_offset = qemu_ram_alloc(ram_size);
    cpu_register_physical_memory(0x10000000, ram_size, ram_offset | IO_MEM_RAM);
    cpu_register_physical_memory(0x90000000, ram_size, ram_offset | IO_MEM_RAM);

    cpu_pic = avr32_pic_init_cpu(env);
    pic = intc_init(0xfff00400, env, cpu_pic);

    at91_usart_init(0xffe00c00, pic[6 * 32], serial_hds[0]);
    at91_usart_init(0xffe01000, pic[7 * 32], serial_hds[1]);
    at91_usart_init(0xffe01400, pic[8 * 32], serial_hds[2]);
    at91_usart_init(0xffe01800, pic[9 * 32], serial_hds[3]);

    counter = qemu_mallocz(sizeof(AVR32Counter));
    counter->timer = qemu_new_timer(vm_clock, avr32_counter_tick, env);
    counter->base = qemu_get_clock(vm_clock);
    /* FIXME get freq based on current clock */
    counter->freq = 20 * 1000000;
    counter->irq = pic[0];

    env->counter = counter;


    if (kernel_filename) {
        uint64_t entry, high;
        int kernel_size;

        kernel_size = load_elf(kernel_filename, 0x0,
                               &entry, NULL, &high, 1, ELF_MACHINE, 0);
        bootstrap_pc = entry;
        if (kernel_size < 0) {
            /* Takes a kimage from the axis devboard SDK.  */
            kernel_size = load_image_targphys(kernel_filename, 0x10000000,
                                              ram_size);
            bootstrap_pc = 0x10000000;

            env->regs[9] = 0x00000000 + kernel_size;
            env->regs[12] = 0xa2a25441;
        }
    }

    env->regs[15] = bootstrap_pc;
}

static QEMUMachine atstk_machine = {
    .name = "atstk1000",
    .desc = "ATSTK 1000 (AT32AP700x)",
    .init = atstk_init,
    .is_default = 1,
};

static void atstk_machine_init(void)
{
    qemu_register_machine(&atstk_machine);
}

machine_init(atstk_machine_init);

