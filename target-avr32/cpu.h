/*
 * AVR32 virtual CPU header
 *
 *  Copyright (c) 2009 Rabin Vincent
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */
#ifndef CPU_AVR32_H
#define CPU_AVR32_H

#define TARGET_LONG_BITS 32

#define CPUState struct CPUAVR32State

#include "cpu-defs.h"

#define TARGET_HAS_ICE 1

#define EXCP_UNRECOVER           1 /* Unrecoverable exception */
#define EXCP_TLB_MULTIPLE_HIT    2 /* TLB multiple hit */
#define EXCP_BUS_ERROR_DATA      3 /* Bus error data fetch */
#define EXCP_BUS_ERROR_INSN      4 /* Bus error instruction fetch */
#define EXCP_NMI                 5 /* NMI */
#define EXCP_INT3                6 /* Interrupt 3 request */
#define EXCP_INT2                7 /* Interrupt 2 request */
#define EXCP_INT1                8 /* Interrupt 1 request */
#define EXCP_INT0                9 /* Interrupt 0 request */
#define EXCP_INSN_ADDR          10 /* Instruction Address */
#define EXCP_ITLB_MISS          11 /* ITLB Miss */
#define EXCP_ITLB_PROTECT       12 /* ITLB Protection */
#define EXCP_BKPT               13 /* Breakpoint */
#define EXCP_ILLEGAL            14 /* Illegal Opcode */
#define EXCP_UNIMP              15 /* Unimplemented instruction */
#define EXCP_PRIVILEGE i        16 /* Privilege violation */
#define EXCP_FP                 17 /* Floating-point */
#define EXCP_COPROC             18 /* Coprocessor absent */
#define EXCP_SVC_CALL           19 /* Supervisor call */
#define EXCP_DTLB_READ_MISS     24
#define EXCP_DTLB_WRITE_MISS    25
#define EXCP_DTLB_READ_PROTECT  26
#define EXCP_DTLB_WRITE_PROTECT 27
#define EXCP_DTLB_MODIFIED      28

#ifdef CONFIG_USER_ONLY
#define EXCP_SYSCALL            99
#endif

#define ELF_MACHINE	EM_AVR32

#define NB_MMU_MODES 2

typedef struct AVR32Counter AVR32Counter;

typedef struct AVR32Tlb {
    uint32_t tlbehi;
    uint32_t tlbelo;
} AVR32Tlb;

typedef struct CPUAVR32State {
    uint32_t regs[16];
    uint32_t sp_usr;
    uint32_t sp_sys;
    uint32_t sysreg[255];

    uint32_t autovector[4];
    AVR32Tlb tlb[32];
    AVR32Counter *counter;

	CPU_COMMON
} CPUAVR32State;

#define SYSREG_SR       0   /* Status Register */
#define SYSREG_EVBA     1   /* Exception Vector Base Address */
#define SYSREG_ACBA     2   /* Application Call Base Address */
#define SYSREG_CPUCR    3   /* CPU Control Register */
#define SYSREG_ECR      4   /* Exception Cause Register */
#define SYSREG_RSR_SUP  5   /* Return Status Register for Supervisor context */
#define SYSREG_RSR_INT0 6   /* Return Status Register for INT 0 context */
#define SYSREG_RSR_INT1 7   /* Return Status Register for INT 1 context */
#define SYSREG_RSR_INT2 8   /* Return Status Register for INT 2 context */
#define SYSREG_RSR_INT3 9   /* Return Status Register for INT 3 context */
#define SYSREG_RSR_EX   10  /* Return Status Register for Exception context */
#define SYSREG_RSR_NMI  11  /* Return Status Register for NMI context */
#define SYSREG_RSR_DBG  12  /* Return Status Register for Debug Mode */
#define SYSREG_RAR_SUP  13  /* Return Address Register for Supervisor context */
#define SYSREG_RAR_INT0 14  /* Return Address Register for INT 0 context */
#define SYSREG_RAR_INT1 15  /* Return Address Register for INT 1 context */
#define SYSREG_RAR_INT2 16  /* Return Address Register for INT 2 context */
#define SYSREG_RAR_INT3 17  /* Return Address Register for INT 3 context */
#define SYSREG_RAR_EX   18  /* Return Address Register for Exception context */
#define SYSREG_RAR_NMI  19  /* Return Address Register for NMI context */
#define SYSREG_RAR_DBG  20  /* Return Address Register for Debug Mode */
#define SYSREG_JECR     21  /* Java Exception Cause Register */
#define SYSREG_JOSP     22  /* Java Operand Stack Pointer */
#define SYSREG_JAVA_LV0 23  /* Java Local Variable 0 */
#define SYSREG_JAVA_LV1 24  /* Java Local Variable 1 */
#define SYSREG_JAVA_LV2 25  /* Java Local Variable 2 */
#define SYSREG_JAVA_LV3 26  /* Java Local Variable 3 */
#define SYSREG_JAVA_LV4 27  /* Java Local Variable 4 */
#define SYSREG_JAVA_LV5 28  /* Java Local Variable 5 */
#define SYSREG_JAVA_LV6 29  /* Java Local Variable 6 */
#define SYSREG_JAVA_LV7 30  /* Java Local Variable 7 */
#define SYSREG_JTBA     31  /* Java Trap Base Address */
#define SYSREG_JBCR     32  /* Java Write Barrier Control Register */
#define SYSREG_CONFIG0  64  /* Configuration register 0 */
#define SYSREG_CONFIG1  65  /* Configuration register 1 */
#define SYSREG_COUNT    66  /* Cycle Counter register */
#define SYSREG_COMPARE  67  /* Compare register */
#define SYSREG_TLBEHI   68  /* MMU TLB Entry High */
#define SYSREG_TLBELO   69  /* MMU TLB Entry Low */
#define SYSREG_PTBR     70  /* MMU Page Table Base Register */
#define SYSREG_TLBEAR   71  /* MMU TLB Exception Address Register */
#define SYSREG_MMUCR    72  /* MMU Control Register */
#define SYSREG_TLBARLO  73  /* MMU TLB Accessed Register Low */
#define SYSREG_TLBARHI  74  /* MMU TLB Accessed Register High */
#define SYSREG_PCCNT    75  /* Performance Clock Counter */
#define SYSREG_PCNT0    76  /* Performance Counter 0 */
#define SYSREG_PCNT1    77  /* Performance Counter 1 */
#define SYSREG_PCCR     78  /* Performance Counter Control Register */
#define SYSREG_BEAR     79  /* Bus Error Address Register */
#define SYSREG_MPUAR0   80  /* MPU Address Register region 0 */
#define SYSREG_MPUAR1   81  /* MPU Address Register region 1 */
#define SYSREG_MPUAR2   82  /* MPU Address Register region 2 */
#define SYSREG_MPUAR3   83  /* MPU Address Register region 3 */
#define SYSREG_MPUAR4   84  /* MPU Address Register region 4 */
#define SYSREG_MPUAR5   85  /* MPU Address Register region 5 */
#define SYSREG_MPUAR6   86  /* MPU Address Register region 6 */
#define SYSREG_MPUAR7   87  /* MPU Address Register region 7 */
#define SYSREG_MPUPSR0  88  /* MPU Privilege Select Register region 0 */
#define SYSREG_MPUPSR1  89  /* MPU Privilege Select Register region 1 */
#define SYSREG_MPUPSR2  90  /* MPU Privilege Select Register region 2 */
#define SYSREG_MPUPSR3  91  /* MPU Privilege Select Register region 3 */
#define SYSREG_MPUPSR4  92  /* MPU Privilege Select Register region 4 */
#define SYSREG_MPUPSR5  93  /* MPU Privilege Select Register region 5 */
#define SYSREG_MPUPSR6  94  /* MPU Privilege Select Register region 6 */
#define SYSREG_MPUPSR7  95  /* MPU Privilege Select Register region 7 */
#define SYSREG_MPUCRA   96  /* MPU Cacheable Register A */
#define SYSREG_MPUCRB   97  /* MPU Cacheable Register B */
#define SYSREG_MPUBRA   98  /* MPU Bufferable Register A */
#define SYSREG_MPUBRB   99  /* MPU Bufferable Register B */
#define SYSREG_MPUAPRA  100 /* MPU Access Permission Register A */
#define SYSREG_MPUAPRB  101 /* MPU Access Permission Register B */
#define SYSREG_MPUCR    102 /* MPU Control Register SYSREG_define   */

#define SR_C    (1 << 0)
#define SR_Z    (1 << 1)
#define SR_N    (1 << 2)
#define SR_V    (1 << 3)
#define SR_Q    (1 << 4)
#define SR_L    (1 << 5)
#define SR_T    (1 << 14)
#define SR_R    (1 << 15)
#define SR_GM   (1 << 16)
#define SR_I0M  (1 << 17)
#define SR_I1M  (1 << 18)
#define SR_I2M  (1 << 19)
#define SR_I3M  (1 << 20)
#define SR_EM   (1 << 21)
#define SR_M0   (1 << 22)
#define SR_M1   (1 << 23)
#define SR_M2   (1 << 24)
#define SR_D    (1 << 26)
#define SR_DM   (1 << 27)
#define SR_J    (1 << 28)
#define SR_H    (1 << 29)

#define SR_M        (0x7 << 22)
#define SR_M_NMI    (SR_M2 | SR_M1 | SR_M0)
#define SR_M_EXP    (SR_M2 | SR_M1 | 0)
#define SR_M_INT3   (SR_M2 | 0     | SR_M0)
#define SR_M_INT2   (SR_M2 | 0     | 0)
#define SR_M_INT1   (0     | SR_M1 | SR_M0)
#define SR_M_INT0   (0     | SR_M1 | 0)
#define SR_M_SUP    (0     | 0     | SR_M0)
#define SR_M_APP    (0     | 0     | 0)

#define TLBEHI_I            (1 << 8)
#define TLBEHI_VALID        (1 << 9)

#define TLBELO_D            (1 << 1)
#define TLBELO_AP_0         (1 << 4)
#define TLBELO_AP_1         (1 << 5)
#define TLBELO_AP_2         (1 << 6)
#define TLBELO_G            (1 << 8)

#define MMUCR_ENABLE        (1 << 0)
#define MMUCR_MODE_SHARED   (1 << 1)
#define MMUCR_INVALIDATE    (1 << 2)
#define MMUCR_NOT_FOUND     (1 << 3)
#define MMUCR_SEGMENT_EN    (1 << 4)
#define MMUCR_DLA_SHIFT     8
#define MMUCR_DRP_SHIFT     14
#define MMUCR_ILA_SHIFT     20
#define MMUCR_IRP_SHIFT     26

#define MMUCR_DLA           (0x3f << 8)
#define MMUCR_DRP           (0x3f << 14)
#define MMUCR_ILA           (0x3f << 20)
#define MMUCR_IRP           (0x3f << 26)

/* MMU modes definitions */
#define MMU_MODE0_SUFFIX _kernel
#define MMU_MODE1_SUFFIX _user
#define MMU_USER_IDX 1
static inline int cpu_mmu_index (CPUState *env)
{
	return 0;
}

#define TARGET_PAGE_BITS 12

#define cpu_init cpu_avr32_init
#define cpu_exec cpu_avr32_exec
#define cpu_gen_code cpu_avr32_gen_code
#define cpu_signal_handler cpu_avr32_signal_handler

#include "cpu-all.h"
#include "exec-all.h"

extern int cpu_avr32_signal_handler(int host_signum, void *pinfo, void *puc);
extern int cpu_avr32_handle_mmu_fault(CPUState *env, target_ulong address, int rw,
                                      int mmu_idx, int is_softmmu);
#define cpu_handle_mmu_fault cpu_avr32_handle_mmu_fault

extern CPUAVR32State *cpu_avr32_init(const char *cpu_model);

static inline void cpu_pc_from_tb(CPUState *env, TranslationBlock *tb)
{
    env->regs[15] = tb->pc;
}

static inline void cpu_get_tb_cpu_state(CPUState *env, target_ulong *pc,
                                        target_ulong *cs_base, int *flags)
{
    *pc = env->regs[15];
}

int cpu_avr32_exec(CPUAVR32State *s);
void raise_exception(int tt);
void avr32_tcg_init(void);
void do_interrupt(CPUState *env);

uint32_t avr32_read_counter(void);
void avr32_write_compare(uint32_t value);

#if defined(CONFIG_USER_ONLY)
static inline void cpu_clone_regs(CPUState *env, target_ulong newsp)
{
    if (newsp)
        env->regs[13] = newsp;
    env->regs[12] = 0;
}
#endif

#endif
