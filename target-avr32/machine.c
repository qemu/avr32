#include "hw/hw.h"
#include "hw/boards.h"

void cpu_save(QEMUFile *f, void *opaque)
{
    CPUAVR32State *env = opaque;
    int i;

    for (i = 0; i < 16; i++)
        qemu_put_be32(f, env->regs[i]);
}

int cpu_load(QEMUFile *f, void *opaque, int version_id)
{
    CPUAVR32State *env = opaque;
    int i;

    for (i = 0; i < 16; i++)
        env->regs[i] = qemu_get_be32(f);

    return 0;
}
