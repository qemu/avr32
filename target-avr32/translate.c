/*
 *  AVR32 translation
 *
 *  Copyright (c) 2009 Rabin Vincent
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <inttypes.h>

#include "cpu.h"
#include "exec-all.h"
#include "helper.h"
#include "disas.h"
#include "tcg-op.h"
#include "qemu-common.h"

#define GEN_HELPER 1
#include "helper.h"

#define EXTRACT_FIELD(src, start, end) \
            (((src) >> start) & ((1 << (end - start + 1)) - 1))

static TCGv_ptr cpu_env;
static TCGv cpu_R[16];
static TCGv cpu_sp_usr;
static TCGv cpu_sp_sys;
TCGv cpu_sysreg[255];

typedef struct DisasContext {
	CPUState *env;
	target_ulong pc, ppc;

    int condjmp;
    int condlabel;

	int is_jmp;
	struct TranslationBlock *tb;
    uint32_t insn;
} DisasContext;

#ifdef DEBUG
int dumping = 1;
#define dbg(format...) if (dumping) fprintf(stderr, format)
#else
static inline int dbg(const char *format, ...)
{
    return 0;
}
#endif

#include "gen-icount.h"

void avr32_tcg_init(void)
{
    static const char *const regnames[] = {
        "r0", "r1", "r2", "r3", "r4", "r5", "r6", "r7", "r8",
        "r9", "r10", "r11", "r12", "r13", "r14", "r15"
    };

    static const char *const sysregnames[] = {
        "SR", "EVBA", "ACBA", "CPUCR", "ECR", "RSR_SUP" "RSR_INT0" "RSR_INT1"
        "RSR_INT2" "RSR_INT3" "RSR_EX" "RSR_NMI" "RSR_DBG" "RAR_SUP" "RAR_INT0"
        "RAR_INT1" "RAR_INT2" "RAR_INT3" "RAR_EX" "RAR_NMI" "RAR_DBG"
    };

    int i;

    cpu_env = tcg_global_reg_new_ptr(TCG_AREG0, "env");
	for (i = 0; i < 16; i++) {
		cpu_R[i] = tcg_global_mem_new(TCG_AREG0,
					      offsetof(CPUState, regs[i]),
					      regnames[i]);
	}

	cpu_sp_usr = tcg_global_mem_new(TCG_AREG0,
					      offsetof(CPUState, sp_usr),
					      "sp_usr");

	cpu_sp_sys = tcg_global_mem_new(TCG_AREG0,
					      offsetof(CPUState, sp_sys),
					      "sp_sys");

	for (i = 0; i < ARRAY_SIZE(sysregnames); i++) {
		cpu_sysreg[i] = tcg_global_mem_new(TCG_AREG0,
					      offsetof(CPUState, sysreg[i]),
					      sysregnames[i]);
	}

	for (; i < ARRAY_SIZE(cpu_sysreg); i++) {
		cpu_sysreg[i] = tcg_global_mem_new(TCG_AREG0,
					      offsetof(CPUState, sysreg[i]),
					      "unknown");
	}

#define GEN_HELPER 2
#include "helper.h"

}

CPUAVR32State *cpu_avr32_init(const char *cpu_model)
{
    CPUAVR32State *env;
    static int inited;

    env = qemu_mallocz(sizeof(CPUAVR32State));
    cpu_exec_init(env);
    if (!inited) {
        inited = 1;
        avr32_tcg_init();
    }

    env->cpu_model_str = cpu_model;

    cpu_reset(env);
    qemu_init_vcpu(env);
    return env;
}

#if defined(CONFIG_USER_ONLY)

int cpu_avr32_handle_mmu_fault(CPUState *env, target_ulong address, int rw,
                                      int mmu_idx, int is_softmmu)
{
    /* FIXME handle */
	cpu_dump_state(env, stderr, fprintf, 0);
	return 1;
}

#else

static int avr32_mmu_translate(CPUState *env, target_ulong address, int rw,
                                target_ulong *phys, int *prot)
{
    uint32_t asid = env->sysreg[SYSREG_TLBEHI] & 0xff;
    uint32_t mmucr = env->sysreg[SYSREG_MMUCR];
    AVR32Tlb *match = NULL;
    int matchindex = 0;
    uint32_t tlbehi;
    uint32_t tlbelo = 0;
    int exception;
    int i;

    dbg("\n%s: env->pc = %p\n", __func__, env->regs[15]);
    dbg("\n%s address=%#lx rw=%d asid=%#lx\n", __func__, address, rw, asid);

    for (i = 0; i < ARRAY_SIZE(env->tlb); i++) {
        AVR32Tlb *tlb = &env->tlb[i];
        tlbehi = tlb->tlbehi;
        tlbelo = tlb->tlbelo;

	if (tlbehi || tlbelo) {
		dbg("[scan] index=%d " \
				"tlbehi=0x%x tlbelo=0x%x "
				"[%c%c%c%c%c]\n",
				i,
				tlbehi,
				tlbelo,
				(tlbehi & TLBEHI_VALID) ? 'V' : 'v',
				(tlbelo & TLBELO_G) ? 'G' : 'g',
				(tlbelo & TLBELO_AP_0) ? 'X' : 'x',
				(tlbelo & TLBELO_AP_1) ? 'W' : 'w',
				(tlbelo & TLBELO_AP_2) ? 'U' : 'u');
	}

        if ((tlbehi & ~0x3ff) == (address & ~0x3ff) &&
            (tlbehi & TLBEHI_VALID)) {

            if (!(mmucr & MMUCR_MODE_SHARED) &&
                !(tlbelo & TLBELO_G) &&
                ((tlbehi & 0xff) != asid))
            {
                continue;
            }

            if (match) {
                dbg("Multiple hit, address=0x%08x mmucr=0x%08x asid=0x%08x! dumping tlb\n", address, mmucr, asid);
                for (i = 0; i < ARRAY_SIZE(env->tlb); i++) {
                    AVR32Tlb *tlb = &env->tlb[i];
                    tlbehi = tlb->tlbehi;
                    tlbelo = tlb->tlbelo;
                    int valid = ((tlbehi & ~0x3ff) == (address & ~0x3ff) && (tlbehi & TLBEHI_VALID));
                    int exclude = (!(mmucr & MMUCR_MODE_SHARED) && !(tlbelo & TLBELO_G) && ((tlbehi & 0xff) != asid));

                    dbg("index=%d tlbehi=0x%x tlbelo=0x%x valid=%d exclude=%d\n", i, tlbehi, tlbelo, valid, exclude);
                }

                cpu_abort(env, "Multiple hit\n");

                exception = EXCP_TLB_MULTIPLE_HIT;
                goto err;
            }

            match = tlb;
            matchindex = i;
        }
    }

    if (!match) {
        static int miss[3] = {EXCP_DTLB_READ_MISS,
                              EXCP_DTLB_WRITE_MISS,
                              EXCP_ITLB_MISS};

        exception = miss[rw];
        /* don't overwrite asid */
        tlbehi = (env->sysreg[SYSREG_TLBEHI] & 0xff) | (address & ~0x3ff);
        goto err;
    }

    tlbehi = match->tlbehi;
    tlbelo = match->tlbelo;

    if (matchindex > 31) {
        env->sysreg[SYSREG_TLBARHI] &= ~(0x80000000u >> (matchindex - 31));
    } else {
        env->sysreg[SYSREG_TLBARLO] &= ~(0x80000000u >> matchindex);
    }

    if ((env->sysreg[SYSREG_SR] & SR_M) == SR_M_APP && !(tlbelo & TLBELO_AP_2)) {
        static int protect[3] = {EXCP_DTLB_READ_PROTECT,
                                 EXCP_DTLB_WRITE_PROTECT,
                                 EXCP_ITLB_PROTECT};

        exception = protect[rw];
        goto err;
    }

    if (!(tlbelo & TLBELO_AP_1)) {
        *prot &= ~PAGE_WRITE;
    }

    if (rw == 1) {
        if (!(tlbelo & TLBELO_AP_1)) {
            exception = EXCP_DTLB_WRITE_PROTECT;
            goto err;
        }

        if (!(tlbelo & TLBELO_D)) {
            exception = EXCP_DTLB_MODIFIED;
            goto err;
        }
    }

    if ((rw == 2) && !(tlbelo & TLBELO_AP_0)) {
        exception = EXCP_ITLB_PROTECT;
        goto err;
    }

    *phys = tlbelo & ~0x3ff;

    dbg("%s WIN  address=0x%x rw=%d phys=0x%x prot=%#x\n", __func__, address, rw, *phys , *prot);

    return 0;

err:
    env->sysreg[SYSREG_TLBEAR] = address;
    env->sysreg[SYSREG_TLBEHI] = tlbehi | TLBEHI_VALID;
    // env->sysreg[SYSREG_TLBELO] = tlbelo;

    env->exception_index = exception;

    if (rw == 2) {
        /* FIXME required? */
        env->sysreg[SYSREG_TLBEHI] |= TLBEHI_I;
    }

    dbg("%s FAIL address=0x%x rw=%d exception=%d tlbehi=0x%x tlbelo=0x%x\n",
        __func__, address, rw, exception, tlbehi, tlbelo);

#if 0
    if (!address) {
        cpu_abort(env, "NULL pointer derefernece\n");
    }
#endif

    return 1;
}
void debug_dump(CPUState *env);

int cpu_avr32_handle_mmu_fault(CPUState *env, target_ulong address, int rw,
                                      int mmu_idx, int is_softmmu)
{
    uint32_t phys_addr;
    int prot = PAGE_READ | PAGE_WRITE;
    int is_user;
    uint32_t mmucr = env->sysreg[SYSREG_MMUCR];
    int segment = mmucr & MMUCR_SEGMENT_EN;
    int paging = mmucr & MMUCR_ENABLE;
    int ret = 0;

    /* FIXME */
    is_user = mmu_idx == MMU_USER_IDX;

    address &= TARGET_PAGE_MASK;

    if (segment) {
        switch (address >> 29) {
            case 0x7: /* P4 */
                phys_addr = address;
                break;

            case 0x6: /* P3 */
                if (paging) {
                    ret = avr32_mmu_translate(env, address, rw, &phys_addr, &prot);
                    break;
                }
                /* fallthru */

            case 0x5: /* P2 */
            case 0x4: /* P1 */
                phys_addr = address &~ (0x7 << 29);
                break;

            default: /* P0/U0 */
                if (paging) {
                    ret = avr32_mmu_translate(env, address, rw, &phys_addr, &prot);
                } else {
                    phys_addr = address;
                }
        }
    } else if (paging) {
        ret = avr32_mmu_translate(env, address, rw, &phys_addr, &prot);
    } else {
        phys_addr = address;
    }

    dbg("%s address = 0x%08x mmu_idx=%d segment=%d paging=%d => phys=0x%08x [ret=%d]\n", __func__, address, mmu_idx, !!segment, paging, phys_addr, ret);

    if (ret) {
        return ret;
    }

    return tlb_set_page (env, address, phys_addr, prot, mmu_idx,
                             is_softmmu);
}

#endif


void debug_dump(CPUState *env)
{
	cpu_dump_state(env, stderr, fprintf, 0);
}

void cpu_dump_state(CPUState *env, FILE *f,
                    int (*cpu_fprintf)(FILE *f, const char *fmt, ...),
                    int flags)
{
    uint32_t asid = env->sysreg[SYSREG_TLBEHI] & 0xff;
    cpu_fprintf(f, "%s, asid = %#x\n", __func__, asid);

    {
        int i;

        for (i = 0; i < 16; i++)
            cpu_fprintf(f, "env->regs[%d] = 0x%08x\n", i, env->regs[i]);
        cpu_fprintf(f, "env->sp_usr = 0x%08x\n", env->sp_usr);
        cpu_fprintf(f, "env->sp_sys = 0x%08x\n", env->sp_sys);
        cpu_fprintf(f, "env->sysreg[SYSREG_SR] = 0x%08x\n", env->sysreg[SYSREG_SR]);
    }

}

static void handle_pc_change(DisasContext *dc, uint32_t rd)
{
    if (rd != 15) {
        return;
    }

    dc->is_jmp = DISAS_JUMP;
}

static int sign_extend(int val, int len)
{
    len = 32 - len;
    return (val << len) >> len;
}

#define COND_EQ     0x0
#define COND_NE     0x1
#define COND_CC     0x2
#define COND_CS     0x3
#define COND_GE     0x4
#define COND_LT     0x5
#define COND_MI     0x6
#define COND_PL     0x7
#define COND_LS     0x8
#define COND_GT     0x9
#define COND_LE     0xa
#define COND_HI     0xb
#define COND_AL     0xf

static void gen_notcond_jump(DisasContext *dc, uint32_t cond, int label)
{
    TCGv temp, temp1;

    if (cond == COND_AL) {
        return;
    }

    temp = tcg_temp_new();

    switch (cond) {
        case COND_EQ:
            tcg_gen_andi_tl(temp, cpu_sysreg[SYSREG_SR], SR_Z);
            tcg_gen_brcondi_tl(TCG_COND_NE, temp, SR_Z, label);
            break;

        case COND_NE:
            tcg_gen_andi_tl(temp, cpu_sysreg[SYSREG_SR], SR_Z);
            tcg_gen_brcondi_tl(TCG_COND_EQ, temp, SR_Z, label);
            break;

        case COND_CC:
            tcg_gen_andi_tl(temp, cpu_sysreg[SYSREG_SR], SR_C);
            tcg_gen_brcondi_tl(TCG_COND_EQ, temp, SR_C, label);
            break;

        case COND_CS:
            tcg_gen_andi_tl(temp, cpu_sysreg[SYSREG_SR], SR_C);
            tcg_gen_brcondi_tl(TCG_COND_NE, temp, SR_C, label);
            break;

        case COND_GE:
            /* Overlay SR_V on top of SR_N */
            tcg_gen_shri_tl(temp, cpu_sysreg[SYSREG_SR], 1);
            tcg_gen_xor_tl(temp, cpu_sysreg[SYSREG_SR], temp);
            tcg_gen_andi_tl(temp, temp, SR_N);
            tcg_gen_brcondi_tl(TCG_COND_NE, temp, 0, label);
            break;

        case COND_LT:
            /* Overlay SR_V on top of SR_N */
            tcg_gen_shri_tl(temp, cpu_sysreg[SYSREG_SR], 1);
            tcg_gen_xor_tl(temp, cpu_sysreg[SYSREG_SR], temp);
            tcg_gen_andi_tl(temp, temp, SR_N);
            tcg_gen_brcondi_tl(TCG_COND_EQ, temp, 0, label);
            break;

        case COND_MI:
            tcg_gen_andi_tl(temp, cpu_sysreg[SYSREG_SR], SR_N);
            tcg_gen_brcondi_tl(TCG_COND_NE, temp, SR_N, label);
            break;

        case COND_PL:
            tcg_gen_andi_tl(temp, cpu_sysreg[SYSREG_SR], SR_N);
            tcg_gen_brcondi_tl(TCG_COND_EQ, temp, SR_N, label);
            break;

        case COND_LS:
            tcg_gen_andi_tl(temp, cpu_sysreg[SYSREG_SR], SR_C | SR_Z);
            tcg_gen_brcondi_tl(TCG_COND_EQ, temp, 0, label);
            break;

        case COND_GT:
            /* Same as COND_LE, except inverted */

            /* Overlay SR_V on top of SR_N */
            tcg_gen_shri_tl(temp, cpu_sysreg[SYSREG_SR], 1);
            tcg_gen_xor_tl(temp, cpu_sysreg[SYSREG_SR], temp);

            /* Overlay SR_Z on top of (SR_V ^ SR_N) */
            temp1 = tcg_temp_new();
            tcg_gen_shli_tl(temp1, cpu_sysreg[SYSREG_SR], 1);
            tcg_gen_or_tl(temp, temp, temp1);
            tcg_temp_free(temp1);

            tcg_gen_andi_tl(temp, temp, SR_N);
            tcg_gen_brcondi_tl(TCG_COND_NE, temp, 0, label);
            break;

        case COND_LE:
            /* Overlay SR_V on top of SR_N */
            tcg_gen_shri_tl(temp, cpu_sysreg[SYSREG_SR], 1);
            tcg_gen_xor_tl(temp, cpu_sysreg[SYSREG_SR], temp);

            /* Overlay SR_Z on top of (SR_V ^ SR_N) */
            temp1 = tcg_temp_new();
            tcg_gen_shli_tl(temp1, cpu_sysreg[SYSREG_SR], 1);
            tcg_gen_or_tl(temp, temp, temp1);
            tcg_temp_free(temp1);

            tcg_gen_andi_tl(temp, temp, SR_N);
            tcg_gen_brcondi_tl(TCG_COND_EQ, temp, 0, label);
            break;

        case COND_HI:
            tcg_gen_andi_tl(temp, cpu_sysreg[SYSREG_SR], SR_C | SR_Z);
            tcg_gen_brcondi_tl(TCG_COND_NE, temp, 0, label);
            break;

        default:
            printf("********** unhandled condition: 0x%04x\n", cond);
    }

    tcg_temp_free(temp);
}

static void gen_cond_skip(DisasContext *dc, uint32_t cond)
{
    if (cond == COND_AL) {
        return;
    }

    dc->condlabel = gen_new_label();
    dc->condjmp = 1;

    gen_notcond_jump(dc, cond, dc->condlabel);
}

static void gen_goto_tb(DisasContext *dc, int n, target_ulong dest)
{
	TranslationBlock *tb;
	tb = dc->tb;

    dbg("%s tb=%p dest=0x%08x\n", __func__, tb, dest);

	if ((tb->pc & TARGET_PAGE_MASK) == (dest & TARGET_PAGE_MASK)) {
		tcg_gen_goto_tb(n);
		tcg_gen_movi_tl(cpu_R[15], dest);
		tcg_gen_exit_tb((long)tb + n);
	} else {
		tcg_gen_movi_tl(cpu_R[15], dest);
		tcg_gen_exit_tb(0);
	}
}

static inline void t_gen_mov_TN_reg(TCGv tn, int r)
{
	if (r < 0 || r > 15)
		fprintf(stderr, "wrong register read $r%d\n", r);

	tcg_gen_mov_tl(tn, cpu_R[r]);
}
static inline void t_gen_mov_reg_TN(int r, TCGv tn)
{
	if (r < 0 || r > 15)
		fprintf(stderr, "wrong register write $r%d\n", r);

	tcg_gen_mov_tl(cpu_R[r], tn);
}

static unsigned int dec_mtsr(DisasContext *dc)
{
    uint32_t r = (dc->insn & 0xf0000) >> 16;
    uint32_t sysreg = dc->insn & 0xff;

    dbg("mtsr r=%d sysreg=%d\n", r, sysreg);

    if (sysreg == SYSREG_COMPARE) {
        gen_helper_write_compare(cpu_R[r]);
        return 4;
    }

    if (sysreg == SYSREG_MMUCR) {
        gen_helper_mmucr_write(cpu_R[r]);
        return 4;
    }

    if (sysreg == SYSREG_TLBEHI || sysreg == SYSREG_TLBELO) {
        gen_helper_tlb_flush();
    }

    tcg_gen_mov_tl(cpu_sysreg[sysreg], cpu_R[r]);

    return 4;
}

static unsigned int dec_mfsr(DisasContext *dc)
{
    uint32_t r = (dc->insn & 0xf0000) >> 16;
    uint32_t sysreg = dc->insn & 0xff;

    dbg("mfsr r=%d systeg=%d\n", r, sysreg);

    if (sysreg == SYSREG_COUNT) {
        gen_helper_read_counter(cpu_R[r]);
        return 4;
    }

    tcg_gen_mov_tl(cpu_R[r], cpu_sysreg[sysreg]);

    return 4;
}

static unsigned int dec_mfdr(DisasContext *dc)
{
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t debugreg = dc->insn & 0xff;

    dbg("mfdr r%u, %u\n", rd, debugreg);

    if (debugreg == 0) {
        tcg_gen_movi_tl(cpu_R[rd],  (0x1e82 << 12) | (0x1f << 1) | 0x1);
    } else {
        printf ("******************* unknown insn pc=%x\n", dc->pc);
    }

    return 4;
}

static unsigned int dec_cache(DisasContext *dc)
{
    /* FIXME */
    dbg("cache\n");

	// tcg_gen_mov_tl(cpu_R[0], cpu_R[0]);

    return 4;
}

static unsigned int dec_sync(DisasContext *dc)
{
    /* FIXME */
    dbg("sync\n");

	// tcg_gen_mov_tl(cpu_R[0], cpu_R[0]);

    return 4;
}

static unsigned int dec_br_disp21(DisasContext *dc)
{
    uint32_t cond = (dc->insn >> 16) & 0xf;
    int disp;

    disp = dc->insn & 0xffff;
    disp |= (dc->insn >> 4) & 0x10000;
    disp |= (dc->insn >> 8) & 0x1e0000;

    disp = sign_extend(disp, 21);

    dbg("br.%u %d\n", cond, disp);

    gen_cond_skip(dc, cond);

    dc->is_jmp = DISAS_JUMP;
	gen_goto_tb(dc, 0, dc->pc + (disp << 1));

    return 4;
}

static unsigned int dec_orh(DisasContext *dc)
{
    uint32_t r = (dc->insn >> 16) & 0xf;
    uint32_t imm = dc->insn & 0xffff;

    dbg("orh r%u %u\n", r, imm);

    tcg_gen_ori_tl(cpu_R[r], cpu_R[r], imm << 16);

    gen_helper_cc_and(cpu_R[r]);

    return 4;
}

static unsigned int dec_orl(DisasContext *dc)
{
    uint32_t r = (dc->insn >> 16) & 0xf;
    uint32_t imm = dc->insn & 0xffff;

    dbg("orl r%u %u\n", r, imm);

    tcg_gen_ori_tl(cpu_R[r], cpu_R[r], imm);

    gen_helper_cc_and(cpu_R[r]);

    return 4;
}

static unsigned int dec_sub_v(DisasContext *dc)
{
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t rs = (dc->insn >> 25) & 0xf;
    uint32_t imm = dc->insn & 0xffff;
    TCGv op1, op2;

    dbg("sub r%u, r%u, %d\n", rd, rs, sign_extend(imm, 16));

    op1 = tcg_temp_new();
    tcg_gen_mov_tl(op1, cpu_R[rs]);
    op2 = tcg_const_tl(sign_extend(imm, 16));

    tcg_gen_subi_tl(cpu_R[rd], cpu_R[rs], sign_extend(imm, 16));

    gen_helper_cc_sub(cpu_R[rd], op1, op2);

    tcg_temp_free(op2);
    tcg_temp_free(op1);

    return 4;
}

static unsigned int dec_sub_imm21(DisasContext *dc)
{
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t imm;
    TCGv op1, op2;

    imm = ((dc->insn >> 25) & 0xf) << 17;
    imm |= ((dc->insn >> 20) & 0x1) << 16;
    imm |= dc->insn & 0xffff;

    dbg("sub r%u, %d\n", rd, sign_extend(imm, 21));

    op1 = tcg_temp_new();
    tcg_gen_mov_tl(op1, cpu_R[rd]);
    op2 = tcg_const_tl(sign_extend(imm, 21));

    tcg_gen_subi_tl(cpu_R[rd], cpu_R[rd], sign_extend(imm, 21));

    gen_helper_cc_sub(cpu_R[rd], op1, op2);

    tcg_temp_free(op2);
    tcg_temp_free(op1);

    return 4;
}

static unsigned int dec_or(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 9) & 0xf;
    uint32_t rd = dc->insn & 0xf;

    dbg("or r%u, r%u\n", rd, rs);

    tcg_gen_or_tl(cpu_R[rd], cpu_R[rd], cpu_R[rs]);

    gen_helper_cc_and(cpu_R[rd]);

    return 2;
}

static unsigned int dec_or_shift(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = (dc->insn) & 0xf;
    uint32_t sa = (dc->insn >> 4) & 0x1f;
    int left = !(dc->insn & (1 << 9));
    TCGv temp;

    dbg("orr r%u, r%u, r%u %s %u\n", rd, rx, ry,
             left ? "<<" : ">>", sa);

    temp = tcg_temp_new();

    if (left) {
        tcg_gen_shli_tl(temp, cpu_R[ry], sa);
    } else {
        tcg_gen_shri_tl(temp, cpu_R[ry], sa);
    }

    tcg_gen_or_tl(cpu_R[rd], cpu_R[rx], temp);

    gen_helper_cc_and(cpu_R[rd]);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_and(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 9) & 0xf;
    uint32_t rd = dc->insn & 0xf;

    dbg("and r%d, r%u\n", rd, rs);

    tcg_gen_and_tl(cpu_R[rd], cpu_R[rd], cpu_R[rs]);
    gen_helper_cc_and(cpu_R[rd]);

    return 2;
}

static unsigned int dec_andn(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 9) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    TCGv temp;

    dbg("andn r%d, r%u\n", rd, rs);

    temp = tcg_temp_new();

    tcg_gen_not_tl(temp, cpu_R[rs]);
    tcg_gen_and_tl(cpu_R[rd], cpu_R[rd], temp);

    gen_helper_cc_and(cpu_R[rd]);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_andhl(DisasContext *dc)
{
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t imm = dc->insn & 0xffff;
    int high = dc->insn & (1 << 26);
    int coh = dc->insn & (1 << 25);
    uint32_t other = coh ? 0 : 0xffff;

    dbg("and%c r%u, 0x%x%s\n",
             high ? 'h' : 'l', rd, imm,
             coh ? ", COH" : "");

    if (high) {
        imm = (imm << 16) | other;
    } else {
        imm = imm | (other << 16);
    }

    tcg_gen_andi_tl(cpu_R[rd], cpu_R[rd], imm);
    gen_helper_cc_and(cpu_R[rd]);

    return 4;
}

static unsigned int dec_and_shift(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t sa = (dc->insn >> 4) & 0x1f;
    uint32_t rd = (dc->insn) & 0xf;
    int left = !(dc->insn & (1 << 9));
    TCGv temp;

    dbg("and r%u, r%u, r%u %s %u\n", rd, rx, ry,
             left ? "<<" : ">>", sa);

    temp = tcg_temp_new();

    if (left) {
        tcg_gen_shli_tl(temp, cpu_R[ry], sa);
    } else {
        tcg_gen_shri_tl(temp, cpu_R[ry], sa);
    }

    tcg_gen_and_tl(cpu_R[rd], cpu_R[rx], temp);
    gen_helper_cc_and(cpu_R[rd]);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_nop(DisasContext *dc)
{
    dbg("nop\n");
    return 2;
}

static unsigned int dec_frs(DisasContext *dc)
{
    dbg("frs\n");
    return 2;
}

static unsigned int dec_tlbs(DisasContext *dc)
{
    dbg("tlbs\n");

    gen_helper_tlbs();

    return 2;
}

static unsigned int dec_tlbw(DisasContext *dc)
{
    dbg("tlbw\n");

    gen_helper_tlbw();

    return 2;
}

static unsigned int dec_tlbr(DisasContext *dc)
{
    dbg("tlbr\n");

    gen_helper_tlbr();

    return 2;
}

static unsigned int dec_pref(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 16) & 0xf;
    uint32_t imm = dc->insn & 0xff;

    dbg("pref r%u[%d]\n", rp, sign_extend(imm, 16));

    return 4;
}

static unsigned int dec_null(DisasContext *dc)
{
    printf ("*********** unknown insn 0x%08x pc=%x\n", dc->insn, dc->pc);

    if (1 || dc->insn == 0x5df0) {
        TCGv temp = tcg_const_tl(EXCP_ILLEGAL);
        gen_helper_raise_exception(temp);
        dc->is_jmp = DISAS_JUMP;
        tcg_temp_free(temp);
    }

#if 0
    dc->is_jmp = DISAS_JUMP;
    gen_goto_tb(dc, 0, dc->pc);
#endif
    fflush(NULL);
    return 2;
}

static unsigned int dec_mov_imm8(DisasContext *dc)
{
    uint32_t imm = (dc->insn >> 4) & 0xff;
    uint32_t rd = dc->insn & 0xf;

    dbg("mov r%d, 0x%08x\n", rd, sign_extend(imm, 8));

    tcg_gen_movi_tl(cpu_R[rd], sign_extend(imm, 8));

    return 2;
}

static unsigned int dec_mov_cond_imm8(DisasContext *dc)
{
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t cond = (dc->insn >> 8) & 0xf;
    uint32_t imm = dc->insn & 0xff;

    dbg("mov.%u r%u, 0x%08x\n", cond, rd, sign_extend(imm, 8));

    gen_cond_skip(dc, cond);
    tcg_gen_movi_tl(cpu_R[rd], sign_extend(imm, 8));

    return 4;
}

static unsigned int dec_mov_imm21(DisasContext *dc)
{
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t imm = dc->insn & 0xffff;

    imm |= ((dc->insn >> 20) & 0x1) << 16;
    imm |= ((dc->insn >> 25) & 0xf) << 17;

    dbg("mov r%d, 0x%08x\n", rd, sign_extend(imm, 21));

    tcg_gen_movi_tl(cpu_R[rd], sign_extend(imm, 21));

    return 4;
}

static unsigned int dec_rjmp(DisasContext *dc)
{
    uint32_t disp = ((dc->insn & 0x3) << 8) | ((dc->insn >> 4) & 0xff);

    dbg("rjmp %d\n", sign_extend(disp, 10) << 1);

    dc->is_jmp = DISAS_JUMP;
    gen_goto_tb(dc, 0, dc->pc + (sign_extend(disp, 10) << 1));

    return 2;
}

static void gen_bit_to_cz(DisasContext *dc, TCGv reg, uint32_t bit)
{
    int l1, l2;
    TCGv temp;

    l1 = gen_new_label();
    l2 = gen_new_label();
    temp = tcg_const_tl(bit);

    tcg_gen_andi_tl(temp, reg, 1 << bit);
    tcg_gen_brcondi_tl(TCG_COND_EQ, temp, 0, l1);
    tcg_temp_free(temp);

    tcg_gen_ori_tl(cpu_sysreg[SYSREG_SR], cpu_sysreg[SYSREG_SR],
                   SR_C | SR_Z);
    tcg_gen_br(l2);

    gen_set_label(l1);
    tcg_gen_andi_tl(cpu_sysreg[SYSREG_SR], cpu_sysreg[SYSREG_SR],
                    ~(SR_C | SR_Z));

    gen_set_label(l2);
}

static unsigned int dec_csrfcz(DisasContext *dc)
{
    uint32_t bit = (dc->insn >> 4) & 0x1f;

    dbg("csrfcz %d\n", bit);

    gen_bit_to_cz(dc, cpu_sysreg[SYSREG_SR], bit);

    return 2;
}

static unsigned int dec_bld(DisasContext *dc)
{
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t bit = dc->insn & 0x1f;

    dbg("bld r%u, %d\n", rd, bit);

    gen_bit_to_cz(dc, cpu_R[rd], bit);

    return 4;
}

static unsigned int dec_bfextu(DisasContext *dc)
{
    uint32_t rd = (dc->insn >> 25) & 0xf;
    uint32_t rs = (dc->insn >> 16) & 0xf;
    uint32_t bp = (dc->insn >> 5) & 0x1f;
    uint32_t w = dc->insn & 0x1f;

    dbg("bfextu r%u, r%u, %u, %u\n", rd, rs, bp, w);

    tcg_gen_shri_tl(cpu_R[rd], cpu_R[rs], bp);
    tcg_gen_andi_tl(cpu_R[rd], cpu_R[rd], ~0u >> (32 - w));

    gen_helper_cc_bfextu(cpu_R[rd]);

    return 4;
}

static unsigned int dec_bfexts(DisasContext *dc)
{
    uint32_t rd = (dc->insn >> 25) & 0xf;
    uint32_t rs = (dc->insn >> 16) & 0xf;
    uint32_t bp = (dc->insn >> 5) & 0x1f;
    uint32_t w = dc->insn & 0x1f;

    dbg("bfexts r%u, r%u, %u, %u\n", rd, rs, bp, w);

    tcg_gen_shri_tl(cpu_R[rd], cpu_R[rs], bp);
    tcg_gen_shli_tl(cpu_R[rd], cpu_R[rd], 32 - w);
    tcg_gen_sari_tl(cpu_R[rd], cpu_R[rd], 32 - w);

    gen_helper_cc_bfextu(cpu_R[rd]);

    return 4;
}

static unsigned int dec_bfins(DisasContext *dc)
{
    uint32_t rd = (dc->insn >> 25) & 0xf;
    uint32_t rs = (dc->insn >> 16) & 0xf;
    uint32_t bp = (dc->insn >> 5) & 0x1f;
    uint32_t w = dc->insn & 0x1f;
    TCGv temp;

    dbg("bfins r%u, r%u, %u, %u\n", rd, rs, bp, w);

    temp = tcg_temp_new();

    tcg_gen_andi_tl(temp, cpu_R[rs], ~0u >> (32 - w));
    tcg_gen_shli_tl(temp, temp, bp);

    tcg_gen_andi_tl(cpu_R[rd], cpu_R[rd], ~((~0u >> (32 - w)) << bp));
    tcg_gen_or_tl(cpu_R[rd], cpu_R[rd], temp);

    gen_helper_cc_bfextu(cpu_R[rd]);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_eor(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 9) & 0xf;
    uint32_t rd = dc->insn & 0xf;

    dbg("eor r%u, r%u\n", rd, rs);

    tcg_gen_xor_tl(cpu_R[rd], cpu_R[rd], cpu_R[rs]);

    gen_helper_cc_and(cpu_R[rd]);

    return 2;
}

static unsigned int dec_eorh(DisasContext *dc)
{
    uint32_t r = (dc->insn >> 16) & 0xf;
    uint32_t imm = dc->insn & 0xffff;

    dbg("eorh r%u, %u\n", r, imm);

    tcg_gen_xori_tl(cpu_R[r], cpu_R[r], imm << 16);

    gen_helper_cc_and(cpu_R[r]);

    return 4;
}

static unsigned int dec_eorl(DisasContext *dc)
{
    uint32_t r = (dc->insn >> 16) & 0xf;
    uint32_t imm = dc->insn & 0xffff;

    dbg("eorl r%u, %u\n", r, imm);

    tcg_gen_xori_tl(cpu_R[r], cpu_R[r], imm);

    gen_helper_cc_and(cpu_R[r]);

    return 4;
}

static unsigned int dec_tst(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 9) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    TCGv temp;

    dbg("tst r%u, r%u\n", rd, rs);

    temp = tcg_temp_new();

    tcg_gen_and_tl(temp, cpu_R[rd], cpu_R[rs]);
    gen_helper_cc_and(temp);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_eor_shift(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t sa = (dc->insn >> 4) & 0x1f;
    uint32_t rd = dc->insn & 0xf;
    int left = !(dc->insn & (1 << 9));
    TCGv temp;

    dbg("eor r%u, r%u, r%u %s %u\n", rd, rx, ry,
            left ? "<<" : ">>", sa);

    temp = tcg_temp_new();

    if (left) {
        tcg_gen_shli_tl(temp, cpu_R[ry], sa);
    } else {
        tcg_gen_shri_tl(temp, cpu_R[ry], sa);
    }

    tcg_gen_xor_tl(cpu_R[rd], cpu_R[rx], temp);

    gen_helper_cc_and(cpu_R[rd]);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_lddpc(DisasContext *dc)
{
	int mem_index = cpu_mmu_index(dc->env);
    uint32_t disp = (dc->insn >> 4) & 0x7f;
    uint32_t r = dc->insn & 0xf;
    uint32_t address;
    TCGv temp;

    address = (dc->pc & 0xfffffffc) + (disp << 2);
    dbg("lddpc r%u, 0x%08x\n", r, address);

    if (r == 15) {
        dc->is_jmp = DISAS_JUMP;
    }

    temp = tcg_const_tl(address);
	tcg_gen_qemu_ld32u(cpu_R[r], temp, mem_index);
    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_br_disp8(DisasContext *dc)
{
    uint32_t cond = dc->insn & 0x7;
    int disp = sign_extend((dc->insn >> 4) & 0xff, 8);

    dbg("br.%u %d\n", cond, disp);

    gen_cond_skip(dc, cond);

    dc->is_jmp = DISAS_JUMP;
	gen_goto_tb(dc, 0, dc->pc + (disp << 1));

    return 2;
}

static unsigned int dec_rsub(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    uint32_t rs = (dc->insn >> 9) & 0xf;
    TCGv temp;

    dbg("rsub r%u, r%d\n", rd, rs);

    temp = tcg_temp_new();

    tcg_gen_sub_tl(temp, cpu_R[rs], cpu_R[rd]);
    gen_helper_cc_sub(temp, cpu_R[rs], cpu_R[rd]);
    tcg_gen_mov_tl(cpu_R[rd], temp);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_rsub_imm8(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 25) & 0xf;
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t imm = dc->insn & 0xff;
    TCGv temp, op1;

    dbg("rsub r%u, r%d, %d\n", rd, rs, sign_extend(imm, 8));

    temp = tcg_temp_new();
    op1 = tcg_const_tl(sign_extend(imm, 8));

    tcg_gen_sub_tl(temp, op1, cpu_R[rs]);
    gen_helper_cc_sub(temp, op1, cpu_R[rs]);
    tcg_gen_mov_tl(cpu_R[rd], temp);

    tcg_temp_free(temp);
    tcg_temp_free(op1);

    return 4;
}

static unsigned int dec_ldm(DisasContext *dc)
{
    uint32_t pp = (dc->insn >> 25) & 0x1;
    uint32_t rp = (dc->insn >> 16) & 0xf;
    uint32_t regs = dc->insn & 0xffff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv loadaddress;
    int i = 14;

    dbg("ldm r%u%s, 0x%0x\n", rp, pp ? "++" : "", regs);

    loadaddress = tcg_temp_new();

    tcg_gen_mov_tl(loadaddress, cpu_R[rp]);

    if (regs & (1 << 15)) {
        if (rp == 15) {
            tcg_gen_mov_tl(loadaddress, cpu_R[13]);
        }

        tcg_gen_qemu_ld32u(cpu_R[15], loadaddress, mem_index);
        tcg_gen_addi_tl(loadaddress, loadaddress, 4);

        dc->is_jmp = DISAS_JUMP;

        if (rp == 15) {
            uint32_t mask = SR_C | SR_V | SR_N | SR_Z;
            uint32_t flags = 0;

            switch (((regs >> 13) & 0x2) | ((regs >> 12) & 0x1)) {
            case 0:
                tcg_gen_movi_tl(cpu_R[12], 0);
                flags = SR_Z;
                break;

            case 1:
                tcg_gen_movi_tl(cpu_R[12], 1);
                break;

            default:
                tcg_gen_movi_tl(cpu_R[12], -1);
                flags = SR_N;
                break;
            }

            tcg_gen_andi_tl(cpu_sysreg[SYSREG_SR], cpu_sysreg[SYSREG_SR], ~mask);
            if (flags) {
                tcg_gen_ori_tl(cpu_sysreg[SYSREG_SR], cpu_sysreg[SYSREG_SR], flags);
            }

            i = 11;
        }
    }

    for (; i >= 0; i--) {
        if (!(regs & (1 << i))) {
            continue;
        }

        tcg_gen_qemu_ld32u(cpu_R[i], loadaddress, mem_index);
        tcg_gen_addi_tl(loadaddress, loadaddress, 4);
    }

    if ((regs & (1 << 15)) && (rp != 15)) {
        gen_helper_cc_ret(cpu_R[12]);
    }

    if (pp) {
        if (rp == 15) {
            tcg_gen_mov_tl(cpu_R[13], loadaddress);
        } else {
            tcg_gen_mov_tl(cpu_R[rp], loadaddress);
        }
    }

    tcg_temp_free(loadaddress);

    return 4;
}

static unsigned int dec_stm(DisasContext *dc)
{
    uint32_t mm = (dc->insn >> 25) & 0x1;
    uint32_t rp = (dc->insn >> 16) & 0xf;
    uint32_t regs = dc->insn & 0xffff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv storeaddress;
    int i;

    dbg("stm %sr%u, 0x%0x\n", mm ? "--" : "", rp, regs);

    storeaddress = tcg_temp_new();

    tcg_gen_mov_tl(storeaddress, cpu_R[rp]);

    if (mm) {
        for (i = 0; i <= 15; i++) {
            if (!(regs & (1 << i))) {
                continue;
            }

            tcg_gen_subi_tl(storeaddress, storeaddress, 4);
            tcg_gen_qemu_st32(cpu_R[i], storeaddress, mem_index);
        }

        tcg_gen_mov_tl(cpu_R[rp], storeaddress);
    } else {
        for (i = 15; i >= 0; i--) {
            if (!(regs & (1 << i))) {
                continue;
            }

            tcg_gen_qemu_st32(cpu_R[i], storeaddress, mem_index);
            tcg_gen_addi_tl(storeaddress, storeaddress, 4);
        }
    }

    tcg_temp_free(storeaddress);

    return 4;
}

static unsigned int dec_stmts(DisasContext *dc)
{
    uint32_t mm = (dc->insn >> 25) & 0x1;
    uint32_t rp = (dc->insn >> 16) & 0xf;
    uint32_t regs = dc->insn & 0xffff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv storeaddress;
    int i;

    dbg("stmts %sr%u, 0x%0x\n", mm ? "--" : "", rp, regs);

    storeaddress = tcg_temp_new();

    /* FIXME banking */

    tcg_gen_mov_tl(storeaddress, cpu_R[rp]);

    if (mm) {
        for (i = 0; i <= 15; i++) {
            if (!(regs & (1 << i))) {
                continue;
            }

            tcg_gen_subi_tl(storeaddress, storeaddress, 4);

            if (i == 13) {
                tcg_gen_qemu_st32(cpu_sp_usr, storeaddress, mem_index);
            } else {
                tcg_gen_qemu_st32(cpu_R[i], storeaddress, mem_index);
            }
        }

        tcg_gen_mov_tl(cpu_R[rp], storeaddress);
    } else {
        for (i = 15; i >= 0; i--) {
            if (!(regs & (1 << i))) {
                continue;
            }

            if (i == 13) {
                tcg_gen_qemu_st32(cpu_sp_usr, storeaddress, mem_index);
            } else {
                tcg_gen_qemu_st32(cpu_R[i], storeaddress, mem_index);
            }

            tcg_gen_addi_tl(storeaddress, storeaddress, 4);
        }
    }

    tcg_temp_free(storeaddress);

    return 4;
}

static unsigned int dec_ldmts(DisasContext *dc)
{
    uint32_t pp = (dc->insn >> 25) & 0x1;
    uint32_t rp = (dc->insn >> 16) & 0xf;
    uint32_t regs = dc->insn & 0xffff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv loadaddress;
    int i;

    dbg("ldmts r%u%s, 0x%0x\n", rp, pp ? "++" : "", regs);

    loadaddress = tcg_temp_new();

    /* FIXME banking */

    tcg_gen_mov_tl(loadaddress, cpu_R[rp]);

    for (i = 15; i >= 0; i--) {
        if (!(regs & (1 << i))) {
            continue;
        }

        if (i == 13) {
            tcg_gen_qemu_ld32u(cpu_sp_usr, loadaddress, mem_index);
        } else {
            tcg_gen_qemu_ld32u(cpu_R[i], loadaddress, mem_index);
        }

        tcg_gen_addi_tl(loadaddress, loadaddress, 4);
    }

    if (pp) {
        tcg_gen_mov_tl(cpu_R[rp], loadaddress);
    }

    tcg_temp_free(loadaddress);

    return 4;
}

static void gen_pushm(int start, int end, TCGv addr, int mem_index)
{
    int i;

    for (i = start; i <= end; i++) {
        tcg_gen_subi_i32(addr, addr, 4);
        tcg_gen_qemu_st32(cpu_R[i], addr, mem_index);
    }
}

static unsigned int dec_pushm(DisasContext *dc)
{
    uint32_t regs = (dc->insn >> 4) & 0xff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv addr;

    dbg("pushm 0x%0x\n", regs);

    addr = tcg_temp_new();
    tcg_gen_mov_tl(addr, cpu_R[13]);

    if (regs & 0x01) {
        gen_pushm(0, 3, addr, mem_index);
    }

    if (regs & 0x02) {
        gen_pushm(4, 7, addr, mem_index);
    }

    if (regs & 0x04) {
        gen_pushm(8, 9, addr, mem_index);
    }

    if (regs & 0x08) {
        gen_pushm(10, 10, addr, mem_index);
    }

    if (regs & 0x10) {
        gen_pushm(11, 11, addr, mem_index);
    }

    if (regs & 0x20) {
        gen_pushm(12, 12, addr, mem_index);
    }

    if (regs & 0x40) {
        gen_pushm(14, 14, addr, mem_index);
    }

    if (regs & 0x80) {
        gen_pushm(15, 15, addr, mem_index);
    }

    tcg_gen_mov_tl(cpu_R[13], addr);
    tcg_temp_free(addr);

    return 2;
}

static void gen_popm(int end, int start, TCGv addr, int mem_index)
{
    int i;

    for (i = end; i >= start; i--) {
        tcg_gen_qemu_ld32u(cpu_R[i], addr, mem_index);
        tcg_gen_addi_i32(addr, addr, 4);
    }
}

static unsigned int dec_popm(DisasContext *dc)
{
    uint32_t regs = (dc->insn >> 4) & 0xff;
    uint32_t k = dc->insn & 0x8;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv addr, jump;

    dbg("popm 0x%0x, %u\n", regs, !!k);

    addr = tcg_temp_new();
    jump = tcg_temp_new();
    tcg_gen_mov_tl(addr, cpu_R[13]);

    if ((regs & 0x80) && k) {
        uint32_t mask = SR_C | SR_V | SR_N | SR_Z;
        uint32_t flags = 0;

        tcg_gen_qemu_ld32u(jump, addr, mem_index);
        tcg_gen_addi_i32(addr, addr, 4);
        dc->is_jmp = DISAS_JUMP;

        switch ((regs & 0x60) >> 5) {
        case 0:
            tcg_gen_movi_tl(cpu_R[12], 0);
            flags = SR_Z;
            break;

        case 1:
            tcg_gen_movi_tl(cpu_R[12], 1);
            break;

        default:
            tcg_gen_movi_tl(cpu_R[12], -1);
            flags = SR_N;
            break;
        }

        tcg_gen_andi_tl(cpu_sysreg[SYSREG_SR], cpu_sysreg[SYSREG_SR], ~mask);
        if (flags) {
            tcg_gen_ori_tl(cpu_sysreg[SYSREG_SR], cpu_sysreg[SYSREG_SR], flags);
        }
    } else {
        if (regs & 0x80) {
            tcg_gen_qemu_ld32u(jump, addr, mem_index);
            tcg_gen_addi_i32(addr, addr, 4);
            dc->is_jmp = DISAS_JUMP;
        }

        if (regs & 0x40) {
            gen_popm(14, 14, addr, mem_index);
        }

        if (regs & 0x20) {
            gen_popm(12, 12, addr, mem_index);
        }

        if (regs & 0x80) {
            gen_helper_cc_ret(cpu_R[12]);
        }
    }

    if (regs & 0x10) {
        gen_popm(11, 11, addr, mem_index);
    }

    if (regs & 0x08) {
        gen_popm(10, 10, addr, mem_index);
    }

    if (regs & 0x04) {
        gen_popm(9, 8, addr, mem_index);
    }

    if (regs & 0x02) {
        gen_popm(7, 4, addr, mem_index);
    }

    if (regs & 0x01) {
        gen_popm(3, 0, addr, mem_index);
    }

    tcg_gen_mov_tl(cpu_R[13], addr);

    if (dc->is_jmp == DISAS_JUMP) {
        tcg_gen_mov_tl(cpu_R[15], jump);
    }

    tcg_temp_free(addr);
    tcg_temp_free(jump);

    return 2;
}

static unsigned int dec_subcond_imm(DisasContext *dc)
{
    uint32_t f = !((dc->insn >> 25) & 0x1); /* f is inverted in manual */
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t cond = (dc->insn >> 8) & 0xf;
    uint32_t imm = dc->insn & 0xff;
    TCGv op1, op2;

    imm = sign_extend(imm, 8);

    dbg("sub%s.%u r%u, %d\n", f ? "f" : "", cond, rd, imm);

    gen_cond_skip(dc, cond);

    op1 = tcg_temp_new();
    tcg_gen_mov_tl(op1, cpu_R[rd]);
    op2 = tcg_const_tl(imm);

    tcg_gen_subi_tl(cpu_R[rd], cpu_R[rd], imm);

    if (f) {
        gen_helper_cc_sub(cpu_R[rd], op1, op2);
    }

    tcg_temp_free(op2);
    tcg_temp_free(op1);

    /* FIXME do this everywhere */
    handle_pc_change(dc, rd);

    return 4;
}

static unsigned int dec_sub_imm8(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    uint32_t imm = (dc->insn >> 4) & 0xff;
    TCGv op1, op2;

    if (rd == 13) {
        imm = sign_extend(imm << 2, 10);
    } else {
        imm = sign_extend(imm, 8);
    }

    dbg("sub r%u, %d\n", rd, imm);

    op1 = tcg_temp_new();
    tcg_gen_mov_tl(op1, cpu_R[rd]);
    op2 = tcg_const_tl(imm);

    tcg_gen_subi_tl(cpu_R[rd], cpu_R[rd], imm);

    gen_helper_cc_sub(cpu_R[rd], op1, op2);

    tcg_temp_free(op2);
    tcg_temp_free(op1);

    /* FIXME do this everywhere */
    handle_pc_change(dc, rd);

    return 2;
}

static unsigned int dec_add(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 9) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    TCGv op1, op2;

    dbg("add r%u, r%u\n", rd, rs);

    op1 = tcg_temp_new();
    op2 = tcg_temp_new();

    tcg_gen_mov_tl(op1, cpu_R[rd]);
    tcg_gen_mov_tl(op2, cpu_R[rs]);

    tcg_gen_add_tl(cpu_R[rd], cpu_R[rd], cpu_R[rs]);

    gen_helper_cc_add(cpu_R[rd], op1, op2);

    tcg_temp_free(op2);
    tcg_temp_free(op1);

    handle_pc_change(dc, rd);

    return 2;
}

static unsigned int dec_add_sa(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t sa = (dc->insn >> 4) & 0x3;
    uint32_t rd = (dc->insn) & 0xf;
    TCGv op1, op2;

    dbg("add r%u, r%u, r%u << %u\n", rd, rx, ry, sa);

    op1 = tcg_temp_new();
    op2 = tcg_temp_new();

    tcg_gen_mov_tl(op1, cpu_R[rx]);
    tcg_gen_shli_tl(op2, cpu_R[ry], sa);

    tcg_gen_add_tl(cpu_R[rd], op1, op2);

    gen_helper_cc_add(cpu_R[rd], op1, op2);

    tcg_temp_free(op2);
    tcg_temp_free(op1);

    if (rd == 15) {
        dc->is_jmp = DISAS_JUMP;
    }

    return 4;
}

static unsigned int dec_adc(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = (dc->insn) & 0xf;
    TCGv op1, op2, c;

    dbg("adc r%u, r%u, r%u\n", rd, rx, ry);

    op1 = tcg_temp_new();
    op2 = tcg_temp_new();
    c = tcg_temp_new();

    tcg_gen_mov_tl(op1, cpu_R[rx]);
    tcg_gen_mov_tl(op2, cpu_R[ry]);

    tcg_gen_add_tl(cpu_R[rd], cpu_R[rx], cpu_R[ry]);

    tcg_gen_andi_tl(c, cpu_sysreg[SYSREG_SR], SR_C);
    tcg_gen_add_tl(cpu_R[rd], cpu_R[rd], c);

    gen_helper_cc_add(cpu_R[rd], op1, op2);

    tcg_temp_free(c);
    tcg_temp_free(op2);
    tcg_temp_free(op1);

    return 4;
}

static unsigned int dec_sbc(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = (dc->insn) & 0xf;
    TCGv op1, op2, c;

    dbg("sbc r%u, r%u, r%u\n", rd, rx, ry);

    op1 = tcg_temp_new();
    op2 = tcg_temp_new();
    c = tcg_temp_new();

    tcg_gen_mov_tl(op1, cpu_R[rx]);
    tcg_gen_mov_tl(op2, cpu_R[ry]);

    tcg_gen_sub_tl(cpu_R[rd], cpu_R[rx], cpu_R[ry]);

    tcg_gen_andi_tl(c, cpu_sysreg[SYSREG_SR], SR_C);
    tcg_gen_sub_tl(cpu_R[rd], cpu_R[rd], c);

    gen_helper_cc_sub(cpu_R[rd], op1, op2);

    tcg_temp_free(c);
    tcg_temp_free(op2);
    tcg_temp_free(op1);

    return 4;
}

static unsigned int dec_sub_rdrs(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 9) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    TCGv op1, op2;

    dbg("sub r%u, r%u\n", rd, rs);

    op1 = tcg_temp_new();
    op2 = tcg_temp_new();

    tcg_gen_mov_tl(op1, cpu_R[rd]);
    tcg_gen_mov_tl(op2, cpu_R[rs]);

    tcg_gen_sub_tl(cpu_R[rd], cpu_R[rd], cpu_R[rs]);

    gen_helper_cc_sub(cpu_R[rd], op1, op2);

    tcg_temp_free(op2);
    tcg_temp_free(op1);

    return 2;
}

static unsigned int dec_sub_sa(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t sa = (dc->insn >> 4) & 0x3;
    uint32_t rd = (dc->insn) & 0xf;
    TCGv op1, op2;

    dbg("sub r%u, r%u, r%u << %u\n", rd, rx, ry, sa);

    op1 = tcg_temp_new();
    op2 = tcg_temp_new();

    tcg_gen_mov_tl(op1, cpu_R[rx]);
    tcg_gen_shli_tl(op2, cpu_R[ry], sa);

    tcg_gen_sub_tl(cpu_R[rd], op1, op2);

    gen_helper_cc_sub(cpu_R[rd], op1, op2);

    tcg_temp_free(op2);
    tcg_temp_free(op1);

    return 4;
}

static unsigned int dec_mov_rdrs(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    uint32_t rs = (dc->insn >> 9) & 0xf;

    dbg("mov r%u, r%u\n", rd, rs);

    tcg_gen_mov_tl(cpu_R[rd], cpu_R[rs]);

    handle_pc_change(dc, rd);

    return 2;
}

static unsigned int dec_mov_cond_rdrs(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 25) & 0xf;
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t cond = (dc->insn >> 4) & 0xf;

    dbg("mov.%u r%u, r%u\n", cond, rd, rs);

    gen_cond_skip(dc, cond);
    tcg_gen_mov_tl(cpu_R[rd], cpu_R[rs]);

    return 4;
}

static unsigned int dec_rcall_disp10(DisasContext *dc)
{
    uint32_t disp = ((dc->insn & 0x3) << 8) | ((dc->insn >> 4) & 0xff);

    dbg("rcall %d\n", sign_extend(disp, 10) << 1);

    dc->is_jmp = DISAS_JUMP;
    tcg_gen_movi_tl(cpu_R[14], dc->pc + 2);
    gen_goto_tb(dc, 0, dc->pc + (sign_extend(disp, 10) << 1));

    return 2;
}

static unsigned int dec_icall(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    dbg("icall r%u\n", rd);

    dc->is_jmp = DISAS_JUMP;

    tcg_gen_mov_tl(cpu_R[15], cpu_R[rd]);
    tcg_gen_movi_tl(cpu_R[14], dc->pc + 2);

    return 2;
}

static unsigned int dec_rcall_disp21(DisasContext *dc)
{
    uint32_t disp = dc->insn & 0xffff;

    disp |= ((dc->insn >> 20) & 0x1) << 16;
    disp |= ((dc->insn >> 25) & 0xf) << 17;

    dbg("rcall %d\n", sign_extend(disp, 21));

    dc->is_jmp = DISAS_JUMP;
    tcg_gen_movi_tl(cpu_R[14], dc->pc + 4);
    gen_goto_tb(dc, 0, dc->pc + (sign_extend(disp, 21) << 1));

    return 4;
}

static unsigned int dec_mcall(DisasContext *dc)
{
    int mem_index = cpu_mmu_index(dc->env);
    uint32_t rd = (dc->insn >> 16) & 0xf;
    int disp = dc->insn & 0xffff;
    TCGv temp;

    disp = sign_extend(disp, 16) << 2;

    dbg("mcall r%u[%d]\n", rd, disp);

    temp = tcg_temp_new();

    dc->is_jmp = DISAS_JUMP;

    tcg_gen_andi_tl(temp, cpu_R[rd], 0xfffffffc);
    tcg_gen_addi_tl(temp, temp, disp);

    tcg_gen_qemu_ld32u(cpu_R[15], temp, mem_index);
    tcg_gen_movi_tl(cpu_R[14], dc->pc + 4);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_stcond(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 25) & 0xf;
    uint32_t rs = (dc->insn >> 16) & 0xf;
    uint32_t disp = dc->insn & 0xffff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("stcond r%u[%d], r%u\n", rp, sign_extend(disp, 16), rs);

    temp = tcg_temp_new();

    /* Move L to Z's position */
    tcg_gen_shri_tl(temp, cpu_sysreg[SYSREG_SR], 4);
    tcg_gen_andi_tl(temp, temp, SR_Z);

    tcg_gen_andi_tl(cpu_sysreg[SYSREG_SR], cpu_sysreg[SYSREG_SR], ~SR_Z);
    tcg_gen_or_tl(cpu_sysreg[SYSREG_SR], cpu_sysreg[SYSREG_SR], temp);

    gen_cond_skip(dc, COND_EQ);

    tcg_gen_addi_i32(temp, cpu_R[rp], sign_extend(disp, 16));
    tcg_gen_qemu_st32(cpu_R[rs], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_stw_pp(DisasContext *dc)
{
    uint32_t rs = dc->insn & 0xf;
    uint32_t rp = (dc->insn >> 9) & 0xf;
    int mem_index = cpu_mmu_index(dc->env);

    dbg("st.w r%u++, r%u\n", rp, rs);

    tcg_gen_qemu_st32(cpu_R[rs], cpu_R[rp], mem_index);
    tcg_gen_addi_i32(cpu_R[rp], cpu_R[rp], 4);

    return 2;
}

static unsigned int dec_stw_mm(DisasContext *dc)
{
    uint32_t rs = dc->insn & 0xf;
    uint32_t rp = (dc->insn >> 9) & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv addr = tcg_temp_new();

    dbg("st.w --r%u, r%u\n", rp, rs);

    tcg_gen_subi_i32(addr, cpu_R[rp], 4);
    tcg_gen_qemu_st32(cpu_R[rs], addr, mem_index);
    tcg_gen_mov_tl(cpu_R[rp], addr);

    tcg_temp_free(addr);

    return 2;
}

static unsigned int dec_stw_disp16(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 16) & 0xf;
    uint32_t rp = (dc->insn >> 25) & 0xf;
    uint32_t disp = dc->insn & 0xffff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("st.w r%u[%d], r%u\n", rp, sign_extend(disp, 16), rs);

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], sign_extend(disp, 16));
    tcg_gen_qemu_st32(cpu_R[rs], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_stw_disp4(DisasContext *dc)
{
    uint32_t rs = dc->insn & 0xf;
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t disp = (dc->insn >> 4) & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("st.w r%u[0x%x], r%u\n", rp, disp << 2, rs);

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], disp << 2);
    tcg_gen_qemu_st32(cpu_R[rs], temp, mem_index);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_stw_shift(DisasContext *dc)
{
    uint32_t rb = (dc->insn >> 25) & 0xf;
    uint32_t ri = (dc->insn >> 16) & 0xf;
    uint32_t sa = (dc->insn >> 4) & 0x3;
    uint32_t rs = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("st.w r%u[r%u << %u], r%u\n", rb, ri, sa, rs);

    temp = tcg_temp_new();

    tcg_gen_shli_tl(temp, cpu_R[ri], sa);
    tcg_gen_add_i32(temp, cpu_R[rb], temp);
    tcg_gen_qemu_st32(cpu_R[rs], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_std_shift(DisasContext *dc)
{
    uint32_t rb = (dc->insn >> 25) & 0xf;
    uint32_t ri = (dc->insn >> 16) & 0xf;
    uint32_t sa = (dc->insn >> 4) & 0x3;
    uint32_t rs = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("st.d r%u[r%u << %u], r%u\n", rb, ri, sa, rs);

    temp = tcg_temp_new();

    tcg_gen_shli_tl(temp, cpu_R[ri], sa);
    tcg_gen_add_i32(temp, cpu_R[rb], temp);

    tcg_gen_qemu_st32(cpu_R[rs + 1], temp, mem_index);

    tcg_gen_addi_i32(temp, temp, 4);
    tcg_gen_qemu_st32(cpu_R[rs], temp, mem_index);

    return 4;
}

static unsigned int dec_stdsp(DisasContext *dc)
{
    uint32_t rs = dc->insn & 0xf;
    uint32_t disp = (dc->insn >> 4) & 0x7f;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    disp <<= 2;

    dbg("stdsp sp[0x%x], r%u\n", disp, rs);

    temp = tcg_temp_new();

    tcg_gen_andi_i32(temp, cpu_R[13], 0xfffffffc);
    tcg_gen_addi_i32(temp, temp, disp);
    tcg_gen_qemu_st32(cpu_R[rs], temp, mem_index);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_lddsp(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    uint32_t disp = (dc->insn >> 4) & 0x7f;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    disp <<= 2;

    dbg("lddsp r%u, sp[0x%x]\n", rd, disp);

    temp = tcg_temp_new();

    tcg_gen_andi_i32(temp, cpu_R[13], 0xfffffffc);
    tcg_gen_addi_i32(temp, temp, disp);
    tcg_gen_qemu_ld32u(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_stb_pp(DisasContext *dc)
{
    uint32_t rs = dc->insn & 0xf;
    uint32_t rp = (dc->insn >> 9) & 0xf;
    int mem_index = cpu_mmu_index(dc->env);

    dbg("st.b r%u++, r%u\n", rp, rs);

    tcg_gen_qemu_st8(cpu_R[rs], cpu_R[rp], mem_index);
    tcg_gen_addi_i32(cpu_R[rp], cpu_R[rp], 1);

    return 2;
}

static unsigned int dec_stb_mm(DisasContext *dc)
{
    uint32_t rs = dc->insn & 0xf;
    uint32_t rp = (dc->insn >> 9) & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv addr = tcg_temp_new();

    dbg("st.b --r%u, r%u\n", rp, rs);

    tcg_gen_subi_i32(addr, cpu_R[rp], 1);
    tcg_gen_qemu_st8(cpu_R[rs], addr, mem_index);
    tcg_gen_mov_tl(cpu_R[rp], addr);

    tcg_temp_free(addr);

    return 2;
}

static unsigned int dec_stb_disp3(DisasContext *dc)
{
    uint32_t rs = dc->insn & 0xf;
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t disp = (dc->insn >> 4) & 0x7;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("st.b r%u[%d], r%u\n", rp, disp, rs);

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], disp);
    tcg_gen_qemu_st8(cpu_R[rs], temp, mem_index);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_stb_disp16(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 25) & 0xf;
    uint32_t rs = (dc->insn >> 16) & 0xf;
    uint32_t disp = dc->insn & 0xffff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    disp = sign_extend(disp, 16);

    dbg("st.b r%u[%d], r%u\n", rp, disp, rs);

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], disp);
    tcg_gen_qemu_st8(cpu_R[rs], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_stb_shift(DisasContext *dc)
{
    uint32_t rb = (dc->insn >> 25) & 0xf;
    uint32_t ri = (dc->insn >> 16) & 0xf;
    uint32_t sa = (dc->insn >> 4) & 0x3;
    uint32_t rs = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("st.b r%u[r%u << %u], r%u\n", rb, ri, sa, rs);

    temp = tcg_temp_new();

    tcg_gen_shli_tl(temp, cpu_R[ri], sa);
    tcg_gen_add_i32(temp, cpu_R[rb], temp);
    tcg_gen_qemu_st8(cpu_R[rs], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_sth_shift(DisasContext *dc)
{
    uint32_t rb = (dc->insn >> 25) & 0xf;
    uint32_t ri = (dc->insn >> 16) & 0xf;
    uint32_t sa = (dc->insn >> 4) & 0x3;
    uint32_t rs = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("st.h r%u[r%u << %u], r%u\n", rb, ri, sa, rs);

    temp = tcg_temp_new();

    tcg_gen_shli_tl(temp, cpu_R[ri], sa);
    tcg_gen_add_i32(temp, cpu_R[rb], temp);
    tcg_gen_qemu_st16(cpu_R[rs], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_sth_pp(DisasContext *dc)
{
    uint32_t rs = dc->insn & 0xf;
    uint32_t rp = (dc->insn >> 9) & 0xf;
    int mem_index = cpu_mmu_index(dc->env);

    dbg("st.h r%u++, r%u\n", rp, rs);

    tcg_gen_qemu_st16(cpu_R[rs], cpu_R[rp], mem_index);
    tcg_gen_addi_i32(cpu_R[rp], cpu_R[rp], 2);

    return 2;
}

static unsigned int dec_sth_mm(DisasContext *dc)
{
    uint32_t rs = dc->insn & 0xf;
    uint32_t rp = (dc->insn >> 9) & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp = tcg_temp_new();

    dbg("st.h --r%u, r%u\n", rp, rs);

    tcg_gen_subi_i32(temp, cpu_R[rp], 2);
    tcg_gen_qemu_st16(cpu_R[rs], temp, mem_index);
    tcg_gen_mov_tl(cpu_R[rp], temp);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_sth_disp3(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t disp = (dc->insn >> 4) & 0x7;
    uint32_t rs = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("st.h r%u[0x%x], r%u\n", rp, disp << 1, rs);

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], disp << 1);
    tcg_gen_qemu_st16(cpu_R[rs], temp, mem_index);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_sth_disp16(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 16) & 0xf;
    uint32_t rp = (dc->insn >> 25) & 0xf;
    uint32_t disp = dc->insn & 0xffff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("st.h r%u[%d], r%u\n", rp, sign_extend(disp, 16), rs);

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], sign_extend(disp, 16));
    tcg_gen_qemu_st16(cpu_R[rs], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_ldw_pp(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);

    dbg("ld.w r%u, r%u++\n", rd, rp);

    tcg_gen_qemu_ld32u(cpu_R[rd], cpu_R[rp], mem_index);
    tcg_gen_addi_i32(cpu_R[rp], cpu_R[rp], 4);

    handle_pc_change(dc, rd);

    return 2;
}

static unsigned int dec_ldw_mm(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp = tcg_temp_new();

    dbg("ld.w r%u, --r%u\n", rd, rp);

    tcg_gen_subi_i32(temp, cpu_R[rp], 4);
    tcg_gen_qemu_ld32u(cpu_R[rd], temp, mem_index);
    tcg_gen_mov_tl(cpu_R[rp], temp);

    tcg_temp_free(temp);

    handle_pc_change(dc, rd);

    return 2;
}

static unsigned int dec_ldw_disp5(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t disp = (dc->insn >> 4) & 0x1f;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("ld.w r%u, r%u[0x%x]\n", rd, rp, disp << 2);

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], disp << 2);
    tcg_gen_qemu_ld32u(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    handle_pc_change(dc, rd);

    return 2;
}

static unsigned int dec_ldw_disp16(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 25) & 0xf;
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t disp = dc->insn & 0xffff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("ld.w r%u, r%u[%d]\n", rd, rp, sign_extend(disp, 16));

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], sign_extend(disp, 16));
    tcg_gen_qemu_ld32u(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    handle_pc_change(dc, rd);

    return 4;
}

static unsigned int dec_ldw_shift(DisasContext *dc)
{
    uint32_t rb = (dc->insn >> 25) & 0xf;
    uint32_t ri = (dc->insn >> 16) & 0xf;
    uint32_t sa = (dc->insn >> 4) & 0x3;
    uint32_t rd = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("ld.w r%u, r%u[r%u << %u]\n", rd, rb, ri, sa);

    temp = tcg_temp_new();

    tcg_gen_shli_tl(temp, cpu_R[ri], sa);
    tcg_gen_add_i32(temp, cpu_R[rb], temp);
    tcg_gen_qemu_ld32u(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    handle_pc_change(dc, rd);

    return 4;
}

static unsigned int dec_ldw_part(DisasContext *dc)
{
    uint32_t rb = (dc->insn >> 25) & 0xf;
    uint32_t ri = (dc->insn >> 16) & 0xf;
    uint32_t part = (dc->insn >> 4) & 0x3;
    uint32_t rd = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("ld.w r%u, r%u[r%u:%u << 2]\n", rd, rb, ri, part);

    temp = tcg_temp_new();

    tcg_gen_shri_tl(temp, cpu_R[ri], 8 * part);
    tcg_gen_andi_tl(temp, temp, 0xff);
    tcg_gen_shli_tl(temp, temp, 2);
    tcg_gen_add_i32(temp, cpu_R[rb], temp);
    tcg_gen_qemu_ld32u(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    handle_pc_change(dc, rd);

    return 4;
}

static unsigned int dec_ldsb_shift(DisasContext *dc)
{
    uint32_t rb = (dc->insn >> 25) & 0xf;
    uint32_t ri = (dc->insn >> 16) & 0xf;
    uint32_t sa = (dc->insn >> 4) & 0x3;
    uint32_t rd = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("ld.sb r%u, r%u[r%u << %u]\n", rd, rb, ri, sa);

    temp = tcg_temp_new();

    tcg_gen_shli_tl(temp, cpu_R[ri], sa);
    tcg_gen_add_i32(temp, cpu_R[rb], temp);
    tcg_gen_qemu_ld8s(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_ldsb_disp16(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 25) & 0xf;
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t disp = dc->insn & 0xffff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("ld.sb r%u, r%u[%d]\n", rd, rp, sign_extend(disp, 16));

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], sign_extend(disp, 16));
    tcg_gen_qemu_ld8s(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_ldub_disp3(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t disp = (dc->insn >> 4) & 0x7;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("ld.ub r%u, r%u[%d]\n", rd, rp, disp);

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], disp);
    tcg_gen_qemu_ld8u(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_ldub_disp16(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 25) & 0xf;
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t disp = dc->insn & 0xffff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("ld.ub r%u, r%u[%d]\n", rd, rp, sign_extend(disp, 16));

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], sign_extend(disp, 16));
    tcg_gen_qemu_ld8u(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_ldub_pp(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);

    dbg("ld.ub r%u, r%u++\n", rd, rp);

    tcg_gen_qemu_ld8u(cpu_R[rd], cpu_R[rp], mem_index);
    tcg_gen_addi_tl(cpu_R[rp], cpu_R[rp], 1);

    return 2;
}

static unsigned int dec_ldub_mm(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);

    dbg("ld.ub r%u, --r%u\n", rd, rp);

    tcg_gen_subi_tl(cpu_R[rp], cpu_R[rp], 1);
    tcg_gen_qemu_ld8u(cpu_R[rd], cpu_R[rp], mem_index);

    return 2;
}

static unsigned int dec_ldub_shift(DisasContext *dc)
{
    uint32_t rb = (dc->insn >> 25) & 0xf;
    uint32_t ri = (dc->insn >> 16) & 0xf;
    uint32_t sa = (dc->insn >> 4) & 0x3;
    uint32_t rd = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("ld.ub r%u, r%u[r%u << %u]\n", rd, rb, ri, sa);

    temp = tcg_temp_new();

    tcg_gen_shli_tl(temp, cpu_R[ri], sa);
    tcg_gen_add_i32(temp, cpu_R[rb], temp);
    tcg_gen_qemu_ld8u(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_ldsh_mm(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp = tcg_temp_new();

    dbg("ld.sh r%u, --r%un", rd, rp);

    tcg_gen_subi_i32(temp, cpu_R[rp], 2);
    tcg_gen_qemu_ld16s(cpu_R[rd], temp, mem_index);
    tcg_gen_movi_tl(cpu_R[rp], temp);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_ldsh_pp(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);

    dbg("ld.sh r%u, r%u++\n", rd, rp);

    tcg_gen_qemu_ld16s(cpu_R[rd], cpu_R[rp], mem_index);
    tcg_gen_addi_i32(cpu_R[rp], cpu_R[rp], 2);

    return 2;
}

static unsigned int dec_ldsh_disp3(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t disp = (dc->insn >> 4) & 0x7;
    uint32_t rd = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("ld.sh r%u, r%u[%d]\n", rd, rp, disp << 1);

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], disp << 1);
    tcg_gen_qemu_ld16s(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_ldsh_disp16(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 25) & 0xf;
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t disp = dc->insn & 0xffff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("ld.sh r%u, r%u[%d]\n", rd, rp, sign_extend(disp, 16));

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], sign_extend(disp, 16));
    tcg_gen_qemu_ld16s(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_lduh_disp3(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t disp = (dc->insn >> 4) & 0x7;
    uint32_t rd = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("ld.uh r%u, r%u[%d]\n", rd, rp, disp << 1);

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], disp << 1);
    tcg_gen_qemu_ld16u(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_ldsh_shift(DisasContext *dc)
{
    uint32_t rb = (dc->insn >> 25) & 0xf;
    uint32_t ri = (dc->insn >> 16) & 0xf;
    uint32_t sa = (dc->insn >> 4) & 0x3;
    uint32_t rd = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("ld.sh r%u, r%u[r%u << %u]\n", rd, rb, ri, sa);

    temp = tcg_temp_new();

    tcg_gen_shli_tl(temp, cpu_R[ri], sa);
    tcg_gen_add_i32(temp, cpu_R[rb], temp);
    tcg_gen_qemu_ld16s(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_lduh_disp16(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 25) & 0xf;
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t disp = dc->insn & 0xffff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("ld.uh r%u, r%u[%d]\n", rd, rp, sign_extend(disp, 16));

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], sign_extend(disp, 16));
    tcg_gen_qemu_ld16u(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_lduh_shift(DisasContext *dc)
{
    uint32_t rb = (dc->insn >> 25) & 0xf;
    uint32_t ri = (dc->insn >> 16) & 0xf;
    uint32_t sa = (dc->insn >> 4) & 0x3;
    uint32_t rd = dc->insn & 0xf;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    dbg("ld.uh r%u, r%u[r%u << %u]\n", rd, rb, ri, sa);

    temp = tcg_temp_new();

    tcg_gen_shli_tl(temp, cpu_R[ri], sa);
    tcg_gen_add_i32(temp, cpu_R[rb], temp);
    tcg_gen_qemu_ld16u(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_ldd_pp(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t rd = ((dc->insn >> 1) & 0x7) << 1;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp = tcg_temp_new();

    dbg("ld.d r%u, r%u++\n", rd, rp);

    tcg_gen_qemu_ld32u(cpu_R[rd + 1], cpu_R[rp], mem_index);
    tcg_gen_addi_i32(temp, cpu_R[rp], 4);

    tcg_gen_qemu_ld32u(cpu_R[rd], temp, mem_index);
    tcg_gen_addi_i32(cpu_R[rp], temp, 4);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_ldd_disp16(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 25) & 0xf;
    uint32_t rd = ((dc->insn >> 17) & 0x7) << 1;
    uint32_t disp = dc->insn & 0xffff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    disp = sign_extend(disp, 16);

    dbg("ld.d r%u, r%u[%d]\n", rd, rp, disp);

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], disp);

    tcg_gen_qemu_ld32u(cpu_R[rd + 1], temp, mem_index);
    tcg_gen_addi_i32(temp, temp, 4);

    tcg_gen_qemu_ld32u(cpu_R[rd], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_std_mm(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t rs = ((dc->insn >> 1) & 0x7) << 1;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp = tcg_temp_new();

    dbg("st.d --r%u, r%u\n", rp, rs);

    tcg_gen_subi_i32(temp, cpu_R[rp], 4);
    tcg_gen_qemu_st32(cpu_R[rs], temp, mem_index);

    tcg_gen_subi_i32(temp, temp, 4);
    tcg_gen_qemu_st32(cpu_R[rs + 1], temp, mem_index);

    tcg_gen_mov_tl(cpu_R[rp], temp);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_std_pp(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 9) & 0xf;
    uint32_t rs = ((dc->insn >> 1) & 0x7) << 1;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp = tcg_temp_new();

    dbg("st.d r%u++, r%u\n", rp, rs);

    tcg_gen_qemu_st32(cpu_R[rs + 1], cpu_R[rp], mem_index);
    tcg_gen_addi_i32(temp, cpu_R[rp], 4);

    tcg_gen_qemu_st32(cpu_R[rs], temp, mem_index);
    tcg_gen_addi_i32(cpu_R[rp], temp, 4);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_std_disp16(DisasContext *dc)
{
    uint32_t rp = (dc->insn >> 25) & 0xf;
    uint32_t rs = ((dc->insn >> 17) & 0x7) << 1;
    uint32_t disp = dc->insn & 0xffff;
    int mem_index = cpu_mmu_index(dc->env);
    TCGv temp;

    disp = sign_extend(disp, 16);

    dbg("st.d r%u[%d], r%u\n", rp, disp, rs);

    temp = tcg_temp_new();

    tcg_gen_addi_i32(temp, cpu_R[rp], disp);

    tcg_gen_qemu_st32(cpu_R[rs + 1], temp, mem_index);
    tcg_gen_addi_i32(temp, temp, 4);

    tcg_gen_qemu_st32(cpu_R[rs], temp, mem_index);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_ret(DisasContext *dc)
{
    uint32_t rs = dc->insn & 0xf;
    uint32_t cond = (dc->insn >> 4) & 0xf;
    uint32_t mask = SR_C | SR_V | SR_N | SR_Z;
    uint32_t flags = 0;

    dbg("ret.%u r%u\n", cond, rs);

    gen_cond_skip(dc, cond);

    switch (rs) {
        case 13:
            tcg_gen_movi_tl(cpu_R[12], 0);
            flags = SR_Z;
            break;

        case 14:
            tcg_gen_movi_tl(cpu_R[12], -1);
            flags = SR_N;
            break;

        case 15:
            tcg_gen_movi_tl(cpu_R[12], 1);
            break;

        default:
            tcg_gen_mov_tl(cpu_R[12], cpu_R[rs]);
            gen_helper_cc_ret(cpu_R[12]);
            break;
    }

    if (rs == 13 || rs == 14 || rs == 15) {
        tcg_gen_andi_tl(cpu_sysreg[SYSREG_SR], cpu_sysreg[SYSREG_SR], ~mask);

        if (flags) {
            tcg_gen_ori_tl(cpu_sysreg[SYSREG_SR], cpu_sysreg[SYSREG_SR], flags);
        }
    }

    dc->is_jmp = DISAS_JUMP;
	tcg_gen_mov_tl(cpu_R[15], cpu_R[14]);

    return 2;
}

static unsigned int dec_cpb(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 25) & 0xf;
    uint32_t rd = (dc->insn >> 16) & 0xf;
    TCGv temp, op1, op2;

    dbg("cp.b r%u, r%u\n", rd, rs);

    temp = tcg_temp_new();
    op1 = tcg_temp_new();
    op2 = tcg_temp_new();

    tcg_gen_andi_tl(op1, cpu_R[rd], 0xff);
    tcg_gen_andi_tl(op2, cpu_R[rs], 0xff);
    tcg_gen_sub_tl(temp, op1, op2);

    gen_helper_cc_subb(temp, op1, op2);

    tcg_temp_free(op2);
    tcg_temp_free(op1);
    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_cph(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 25) & 0xf;
    uint32_t rd = (dc->insn >> 16) & 0xf;
    TCGv temp, op1, op2;

    dbg("cp.h r%u, r%u\n", rd, rs);

    temp = tcg_temp_new();
    op1 = tcg_temp_new();
    op2 = tcg_temp_new();

    tcg_gen_andi_tl(op1, cpu_R[rd], 0xffff);
    tcg_gen_andi_tl(op2, cpu_R[rs], 0xffff);
    tcg_gen_sub_tl(temp, op1, op2);

    gen_helper_cc_subh(temp, op1, op2);

    tcg_temp_free(op2);
    tcg_temp_free(op1);
    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_cpw_imm6(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    uint32_t imm = (dc->insn >> 4) & 0x3f;
    TCGv temp, op2;

    dbg("cp.w r%u, %d\n", rd, sign_extend(imm, 6));

    temp = tcg_temp_new();
    op2 = tcg_const_tl(sign_extend(imm, 6));

    tcg_gen_subi_tl(temp, cpu_R[rd], sign_extend(imm, 6));

    gen_helper_cc_sub(temp, cpu_R[rd], op2);

    tcg_temp_free(op2);
    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_cpw_imm21(DisasContext *dc)
{
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t imm = dc->insn & 0xffff;
    TCGv temp, op2;

    imm |= ((dc->insn >> 20) & 0x1) << 16;
    imm |= ((dc->insn >> 25) & 0xf) << 17;

    dbg("cp.w r%u, %d\n", rd, sign_extend(imm, 21));

    temp = tcg_temp_new();
    op2 = tcg_const_tl(sign_extend(imm, 21));

    tcg_gen_subi_tl(temp, cpu_R[rd], sign_extend(imm, 21));

    gen_helper_cc_sub(temp, cpu_R[rd], op2);

    tcg_temp_free(op2);
    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_cpw(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 9) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    TCGv temp;

    dbg("cp.w r%u, r%u\n", rd, rs);

    temp = tcg_temp_new();

    tcg_gen_sub_tl(temp, cpu_R[rd], cpu_R[rs]);
    gen_helper_cc_sub(temp, cpu_R[rd], cpu_R[rs]);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_brev(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;

    dbg("brev r%u\n", rd);

    gen_helper_brev(cpu_R[rd], cpu_R[rd]);

    return 2;
}

static unsigned int dec_swap_b(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;

    dbg("swap.b r%u\n", rd);

    tcg_gen_bswap32_tl(cpu_R[rd], cpu_R[rd]);

    return 2;
}

static unsigned int dec_mustr(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;

    dbg("mustr r%u\n", rd);

    tcg_gen_andi_tl(cpu_R[rd], cpu_sysreg[SYSREG_SR], 0xf);

    return 2;
}

static unsigned int dec_ror(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;

    dbg("ror r%u\n", rd);

    gen_helper_ror(cpu_R[rd], cpu_R[rd]);

    return 2;
}

static unsigned int dec_rol(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;

    dbg("rol r%u\n", rd);

    gen_helper_rol(cpu_R[rd], cpu_R[rd]);

    return 2;
}

static unsigned int dec_acr(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;

    dbg("acr r%u\n", rd);

    gen_helper_acr(cpu_R[rd], cpu_R[rd]);

    return 2;
}

static unsigned int dec_clz(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 25) & 0xf;
    uint32_t rd = (dc->insn >> 16) & 0xf;

    dbg("clz r%u, r%u\n", rd, rs);

    gen_helper_clz(cpu_R[rd], cpu_R[rs]);

    return 4;
}

static unsigned int dec_sbr(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    uint32_t bp;

    bp = ((dc->insn >> 9) & 0xf) << 1;
    bp |= (dc->insn >> 4) & 0x1;

    dbg("sbr r%u, %u\n", rd, bp);

    tcg_gen_ori_i32(cpu_R[rd], cpu_R[rd], 1 << bp);
    tcg_gen_andi_i32(cpu_sysreg[SYSREG_SR], cpu_sysreg[SYSREG_SR], ~SR_Z);

    return 2;
}

static unsigned int dec_cbr(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    uint32_t bp;
    int zero, out;

    bp = ((dc->insn >> 9) & 0xf) << 1;
    bp |= (dc->insn >> 4) & 0x1;

    dbg("cbr r%u, %u\n", rd, bp);

    tcg_gen_andi_i32(cpu_R[rd], cpu_R[rd], ~(1 << bp));

    zero = gen_new_label();
    out = gen_new_label();

    tcg_gen_brcondi_tl(TCG_COND_EQ, cpu_R[rd], 0, zero);
    tcg_gen_andi_i32(cpu_sysreg[SYSREG_SR], cpu_sysreg[SYSREG_SR], ~SR_Z);
    tcg_gen_br(out);

    gen_set_label(zero);
    tcg_gen_ori_i32(cpu_sysreg[SYSREG_SR], cpu_sysreg[SYSREG_SR], SR_Z);

    gen_set_label(out);

    return 2;
}

static unsigned int dec_csrf(DisasContext *dc)
{
    uint32_t bp = (dc->insn >> 4) & 0x1f;

    dbg("csrf %u\n", bp);

    tcg_gen_andi_i32(cpu_sysreg[SYSREG_SR], cpu_sysreg[SYSREG_SR], ~(1 << bp));

    return 2;
}

static unsigned int dec_ssrf(DisasContext *dc)
{
    uint32_t bp = (dc->insn >> 4) & 0x1f;

    dbg("ssrf %u\n", bp);

    tcg_gen_ori_i32(cpu_sysreg[SYSREG_SR], cpu_sysreg[SYSREG_SR], 1 << bp);

    return 2;
}

static unsigned int dec_tbnz(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;

    dbg("tbnz r%u\n", rd);

    gen_helper_tbnz(cpu_R[rd]);

    return 2;
}

static unsigned int dec_com(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;

    dbg("com r%u\n", rd);

    tcg_gen_not_tl(cpu_R[rd], cpu_R[rd]);

    gen_helper_cc_com(cpu_R[rd]);

    return 2;
}

static unsigned int dec_neg(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    TCGv temp;

    dbg("neg r%u\n", rd);

    temp = tcg_temp_new();
    tcg_gen_mov_tl(temp, cpu_R[rd]);

    tcg_gen_not_tl(cpu_R[rd], cpu_R[rd]);
    tcg_gen_addi_tl(cpu_R[rd], cpu_R[rd], 1);

    gen_helper_cc_neg(cpu_R[rd], temp);

    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_abs(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    int out;

    dbg("abs r%u\n", rd);

    out = gen_new_label();

    tcg_gen_brcondi_tl(TCG_COND_GE, cpu_R[rd], 0, out);

    tcg_gen_not_tl(cpu_R[rd], cpu_R[rd]);
    tcg_gen_addi_tl(cpu_R[rd], cpu_R[rd], 1);

    gen_set_label(out);

    gen_helper_cc_com(cpu_R[rd]);

    return 2;
}

static unsigned int dec_castu(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    int half = dc->insn & (1 << 5);

    dbg("castu.%c r%u\n", half ? 'h' : 'b', rd);

    tcg_gen_andi_i32(cpu_R[rd], cpu_R[rd], half ? 0xffff : 0xff);
    gen_helper_cc_castu(cpu_R[rd]);

    return 2;
}

static unsigned int dec_asr(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    uint32_t sa;
    TCGv op, shamt;

    sa = ((dc->insn >> 9) & 0xf) << 1;
    sa |= (dc->insn >> 4) & 0x1;

    dbg("asr r%u, %u\n", rd, sa);

    op = tcg_temp_new();
    shamt = tcg_const_tl(sa);
    tcg_gen_mov_tl(op, cpu_R[rd]);

    tcg_gen_sari_tl(cpu_R[rd], cpu_R[rd], sa);

    gen_helper_cc_lsr(cpu_R[rd], op, shamt);

    tcg_temp_free(shamt);
    tcg_temp_free(op);

    return 2;
}

static unsigned int dec_asr_rs(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 25) & 0xf;
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t sa = dc->insn & 0x1f;
    TCGv op, shamt;

    dbg("asr r%u, r%u, %u\n", rd, rs, sa);

    op = tcg_temp_new();
    shamt = tcg_const_tl(sa);
    tcg_gen_mov_tl(op, cpu_R[rs]);

    tcg_gen_sari_tl(cpu_R[rd], cpu_R[rs], sa);

    gen_helper_cc_lsr(cpu_R[rd], op, shamt);

    tcg_temp_free(shamt);
    tcg_temp_free(op);

    return 4;
}

static unsigned int dec_asr_rxry(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    TCGv op, shamt;

    dbg("asr r%u, r%u, r%u\n", rd, rx, ry);

    op = tcg_temp_new();
    shamt = tcg_temp_new();

    tcg_gen_mov_tl(op, cpu_R[rx]);
    tcg_gen_andi_tl(shamt, cpu_R[ry], 0x1f);

    tcg_gen_sar_tl(cpu_R[rd], op, shamt);

    gen_helper_cc_lsr(cpu_R[rd], op, shamt);

    tcg_temp_free(shamt);
    tcg_temp_free(op);

    return 4;
}

static unsigned int dec_lsr_rs(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 25) & 0xf;
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t sa = dc->insn & 0x1f;
    TCGv op, shamt;

    dbg("lsr r%u, r%u, %u\n", rd, rs, sa);

    op = tcg_temp_new();
    shamt = tcg_const_tl(sa);
    tcg_gen_mov_tl(op, cpu_R[rs]);

    tcg_gen_shri_tl(cpu_R[rd], cpu_R[rs], sa);

    gen_helper_cc_lsr(cpu_R[rd], op, shamt);

    tcg_temp_free(shamt);
    tcg_temp_free(op);

    return 4;
}

static unsigned int dec_lsr(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    uint32_t sa;
    TCGv op, shamt;

    sa = ((dc->insn >> 9) & 0xf) << 1;
    sa |= (dc->insn >> 4) & 0x1;

    dbg("lsr r%u, %u\n", rd, sa);

    op = tcg_temp_new();
    shamt = tcg_const_tl(sa);
    tcg_gen_mov_tl(op, cpu_R[rd]);

    tcg_gen_shri_tl(cpu_R[rd], cpu_R[rd], sa);

    gen_helper_cc_lsr(cpu_R[rd], op, shamt);

    tcg_temp_free(shamt);
    tcg_temp_free(op);

    return 2;
}

static unsigned int dec_lsr_rxry(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    TCGv op, shamt;

    dbg("lsr r%u, r%u, r%u\n", rd, rx, ry);

    op = tcg_temp_new();
    shamt = tcg_temp_new();

    tcg_gen_mov_tl(op, cpu_R[rx]);
    tcg_gen_andi_tl(shamt, cpu_R[ry], 0x1f);

    tcg_gen_shr_tl(cpu_R[rd], op, shamt);

    gen_helper_cc_lsr(cpu_R[rd], op, shamt);

    tcg_temp_free(shamt);
    tcg_temp_free(op);

    return 4;
}

static unsigned int dec_lsl(DisasContext *dc)
{
    uint32_t rd = dc->insn & 0xf;
    uint32_t sa;
    TCGv op, shamt;

    sa = ((dc->insn >> 9) & 0xf) << 1;
    sa |= (dc->insn >> 4) & 0x1;

    dbg("lsl r%u, %u\n", rd, sa);

    op = tcg_temp_new();
    shamt = tcg_const_tl(sa);
    tcg_gen_mov_tl(op, cpu_R[rd]);

    tcg_gen_shli_tl(cpu_R[rd], cpu_R[rd], sa);

    gen_helper_cc_lsl(cpu_R[rd], op, shamt);

    tcg_temp_free(shamt);
    tcg_temp_free(op);

    return 2;
}

static unsigned int dec_lsl_rs(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 25) & 0xf;
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t sa = dc->insn & 0x1f;
    TCGv op, shamt;

    dbg("lsl r%u, r%u, %u\n", rd, rs, sa);

    op = tcg_temp_new();
    shamt = tcg_const_tl(sa);
    tcg_gen_mov_tl(op, cpu_R[rs]);

    tcg_gen_shl_tl(cpu_R[rd], op, shamt);

    gen_helper_cc_lsl(cpu_R[rd], op, shamt);

    tcg_temp_free(shamt);
    tcg_temp_free(op);

    return 4;
}

static unsigned int dec_lsl_rxry(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    TCGv op, shamt;

    dbg("lsl r%u, r%u, r%u\n", rd, rx, ry);

    op = tcg_temp_new();
    shamt = tcg_temp_new();

    tcg_gen_mov_tl(op, cpu_R[rx]);
    tcg_gen_andi_tl(shamt, cpu_R[ry], 0x1f);

    tcg_gen_shl_tl(cpu_R[rd], op, shamt);

    gen_helper_cc_lsl(cpu_R[rd], op, shamt);

    tcg_temp_free(shamt);
    tcg_temp_free(op);

    return 4;
}

static unsigned int dec_divu(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    TCGv temp;

    dbg("divu r%u, r%u, r%u\n", rd, rx, ry);

    temp = tcg_temp_new();

    tcg_gen_divu_tl(temp, cpu_R[rx], cpu_R[ry]);
    tcg_gen_remu_tl(cpu_R[rd + 1], cpu_R[rx], cpu_R[ry]);

    tcg_gen_mov_tl(cpu_R[rd], temp);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_divs(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    TCGv temp;

    dbg("divs r%u, r%u, r%u\n", rd, rx, ry);

    temp = tcg_temp_new();

    tcg_gen_div_tl(temp, cpu_R[rx], cpu_R[ry]);
    tcg_gen_rem_tl(cpu_R[rd + 1], cpu_R[rx], cpu_R[ry]);

    tcg_gen_mov_tl(cpu_R[rd], temp);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_mac(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    TCGv temp;

    dbg("mac r%u, r%u, r%u\n", rd, rx, ry);

    temp = tcg_temp_new();

    tcg_gen_mul_tl(temp, cpu_R[rx], cpu_R[ry]);
    tcg_gen_add_tl(cpu_R[rd], temp, cpu_R[rd]);

    tcg_temp_free(temp);

    return 4;
}

static unsigned int dec_mul(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 9) & 0xf;
    uint32_t rd = dc->insn & 0xf;

    dbg("mul r%u, r%u\n", rd, rs);

    tcg_gen_mul_tl(cpu_R[rd], cpu_R[rd], cpu_R[rs]);

    return 2;
}

static unsigned int dec_mul_rxry(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = dc->insn & 0xf;

    dbg("mul r%u, r%u, r%u\n", rd, rx, ry);

    tcg_gen_mul_tl(cpu_R[rd], cpu_R[rx], cpu_R[ry]);

    return 4;
}

static unsigned int dec_mul_rdrs(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 25) & 0xf;
    uint32_t rd = (dc->insn >> 16) & 0xf;
    uint32_t imm = dc->insn & 0xff;

    dbg("mul r%u, r%u, %d\n", rd, rs, sign_extend(imm, 8));

    tcg_gen_muli_tl(cpu_R[rd], cpu_R[rs], sign_extend(imm, 8));

    return 4;
}

static unsigned int dec_mulud(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    TCGv_i64 tmp1 = tcg_temp_new_i64();
    TCGv_i64 tmp2 = tcg_temp_new_i64();

    dbg("mulu.d r%u, r%u, r%u\n", rd, rx, ry);

    tcg_gen_extu_i32_i64(tmp1, cpu_R[rx]);
    tcg_gen_extu_i32_i64(tmp2, cpu_R[ry]);

    tcg_gen_mul_i64(tmp1, tmp1, tmp2);

    tcg_gen_trunc_i64_i32(cpu_R[rd], tmp1);
    tcg_gen_shri_i64(tmp1, tmp1, 32);
    tcg_gen_trunc_i64_i32(cpu_R[rd + 1], tmp1);

    tcg_temp_free_i64(tmp2);
    tcg_temp_free_i64(tmp1);

    return 4;
}

static unsigned int dec_mulsd(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    TCGv_i64 tmp1 = tcg_temp_new_i64();
    TCGv_i64 tmp2 = tcg_temp_new_i64();

    dbg("muls.d r%u, r%u, r%u\n", rd, rx, ry);

    tcg_gen_ext_i32_i64(tmp1, cpu_R[rx]);
    tcg_gen_ext_i32_i64(tmp2, cpu_R[ry]);

    tcg_gen_mul_i64(tmp1, tmp1, tmp2);

    tcg_gen_trunc_i64_i32(cpu_R[rd], tmp1);
    tcg_gen_shri_i64(tmp1, tmp1, 32);
    tcg_gen_trunc_i64_i32(cpu_R[rd + 1], tmp1);

    tcg_temp_free_i64(tmp2);
    tcg_temp_free_i64(tmp1);

    return 4;
}

static unsigned int dec_macud(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    TCGv_i64 tmp1 = tcg_temp_new_i64();
    TCGv_i64 tmp2 = tcg_temp_new_i64();
    TCGv_i64 acc = tcg_temp_new_i64();

    dbg("macu.d r%u, r%u, r%u\n", rd, rx, ry);

    tcg_gen_extu_i32_i64(acc, cpu_R[rd + 1]);
    tcg_gen_extu_i32_i64(tmp1, cpu_R[rd]);
    tcg_gen_shli_i64(acc, acc, 32);
    tcg_gen_or_i64(acc, acc, tmp1);

    tcg_gen_extu_i32_i64(tmp1, cpu_R[rx]);
    tcg_gen_extu_i32_i64(tmp2, cpu_R[ry]);

    tcg_gen_mul_i64(tmp1, tmp1, tmp2);
    tcg_gen_add_i64(acc, acc, tmp1);

    tcg_gen_trunc_i64_i32(cpu_R[rd], acc);
    tcg_gen_shri_i64(acc, acc, 32);
    tcg_gen_trunc_i64_i32(cpu_R[rd + 1], acc);

    tcg_temp_free_i64(acc);
    tcg_temp_free_i64(tmp2);
    tcg_temp_free_i64(tmp1);

    return 4;
}

static unsigned int dec_sr(DisasContext *dc)
{
    uint32_t cond = (dc->insn >> 4) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    int zero, out;

    dbg("sr.%u r%u\n", cond, rd);

    zero = gen_new_label();
    out = gen_new_label();

    gen_notcond_jump(dc, cond, zero);
    tcg_gen_movi_tl(cpu_R[rd], 1);
    tcg_gen_br(out);

    gen_set_label(zero);
    tcg_gen_movi_tl(cpu_R[rd], 0);

    gen_set_label(out);

    return 2;
}

static unsigned int dec_max(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    int xismax, out;

    dbg("max r%u, r%u, r%u\n", rd, rx, ry);

    xismax = gen_new_label();
    out = gen_new_label();

    tcg_gen_brcond_tl(TCG_COND_GT, cpu_R[rx], cpu_R[ry], xismax);

    tcg_gen_mov_tl(cpu_R[rd], cpu_R[ry]);
    tcg_gen_br(out);

    gen_set_label(xismax);
    tcg_gen_mov_tl(cpu_R[rd], cpu_R[rx]);

    gen_set_label(out);

    return 4;
}

static unsigned int dec_min(DisasContext *dc)
{
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = dc->insn & 0xf;
    int xismax, out;

    dbg("min r%u, r%u, r%u\n", rd, rx, ry);

    xismax = gen_new_label();
    out = gen_new_label();

    tcg_gen_brcond_tl(TCG_COND_GT, cpu_R[rx], cpu_R[ry], xismax);

    tcg_gen_mov_tl(cpu_R[rd], cpu_R[rx]);
    tcg_gen_br(out);

    gen_set_label(xismax);
    tcg_gen_mov_tl(cpu_R[rd], cpu_R[ry]);

    gen_set_label(out);

    return 4;
}

static unsigned int dec_cpc_rdrs(DisasContext *dc)
{
    uint32_t rs = (dc->insn >> 25) & 0xf;
    uint32_t rd = (dc->insn >> 16) & 0xf;
    TCGv temp, c;

    dbg("cpc r%u, r%u\n", rd, rs);

    temp = tcg_temp_new();
    c = tcg_temp_new();

    tcg_gen_sub_tl(temp, cpu_R[rd], cpu_R[rs]);

    tcg_gen_andi_tl(c, cpu_sysreg[SYSREG_SR], SR_C);
    tcg_gen_sub_tl(temp, temp, c);

    gen_helper_cc_cpc(temp, cpu_R[rd], cpu_R[rs]);

    tcg_temp_free(c);

    return 4;
}

static unsigned int dec_cpc(DisasContext *dc)
{
    uint32_t rd = (dc->insn) & 0xf;
    TCGv temp, op2, c;

    dbg("cpc r%u\n", rd);

    temp = tcg_temp_new();
    op2 = tcg_const_tl(0);
    c = tcg_temp_new();

    tcg_gen_sub_tl(temp, cpu_R[rd], op2);

    tcg_gen_andi_tl(c, cpu_sysreg[SYSREG_SR], SR_C);
    tcg_gen_sub_tl(temp, temp, c);

    gen_helper_cc_cpc(temp, cpu_R[rd], op2);

    tcg_temp_free(c);
    tcg_temp_free(op2);
    tcg_temp_free(temp);

    return 2;
}

static unsigned int dec_xchg(DisasContext *dc)
{
    int mem_index = cpu_mmu_index(dc->env);
    uint32_t rx = (dc->insn >> 25) & 0xf;
    uint32_t ry = (dc->insn >> 16) & 0xf;
    uint32_t rd = dc->insn & 0xf;

    dbg("xchg r%u, r%u, r%u\n", rd, rx, ry);

    tcg_gen_qemu_ld32u(cpu_R[rd], cpu_R[rx], mem_index);
    tcg_gen_qemu_st32(cpu_R[ry], cpu_R[rx], mem_index);

    return 4;
}

static unsigned int dec_rete(DisasContext *dc)
{
    dbg("rete\n");

    gen_helper_rete();
    dc->is_jmp = DISAS_JUMP;

    return 2;
}

static unsigned int dec_rets(DisasContext *dc)
{
    dbg("rets\n");

    gen_helper_rets();
    dc->is_jmp = DISAS_JUMP;

    return 2;
}

static unsigned int dec_scall(DisasContext *dc)
{
    dbg("scall\n");

    gen_helper_scall();
    dc->is_jmp = DISAS_JUMP;

    return 2;
}

typedef struct decoder_info {
	uint32_t bits;
	uint32_t mask;
	unsigned int (*dec)(DisasContext *dc);
} DecoderInfo;

static DecoderInfo decinfo_32[] = {
    {0xe3b00000, 0xfff0ff00, dec_mtsr},
    {0xe1b00000, 0xfff0ff00, dec_mfsr},
    {0xe5b00000, 0xfff0ff00, dec_mfdr},
    {0xf4100000, 0xfff00000, dec_cache},
    {0xebb00000, 0xffffff00, dec_sync},
    {0xe0800000, 0xe1e00000, dec_br_disp21},
    {0xea100000, 0xfff00000, dec_orh},
    {0xe8100000, 0xfff00000, dec_orl},
    {0xee100000, 0xfff00000, dec_eorh},
    {0xec100000, 0xfff00000, dec_eorl},
    {0xe1c00000, 0xfdf00000, dec_ldm},
    {0xe9c00000, 0xfdf00000, dec_stm},
    {0xe0c00000, 0xe1f00000, dec_sub_v},
    {0xe1e01000, 0xe1f0fc00, dec_or_shift},
    {0xe0100000, 0xf9f00000, dec_andhl},
    {0xe0a00000, 0xe1ef0000, dec_rcall_disp21},
    {0xe1700000, 0xe1f00000, dec_stcond},
    {0xe1000000, 0xe1f00000, dec_ldsh_disp16},
    {0xe1100000, 0xe1f00000, dec_lduh_disp16},
    {0xe1200000, 0xe1f00000, dec_ldsb_disp16},
    {0xe1300000, 0xe1f00000, dec_ldub_disp16},
    {0xe1400000, 0xe1f00000, dec_stw_disp16},
    {0xe1500000, 0xe1f00000, dec_sth_disp16},
    {0xe0f00000, 0xe1f00000, dec_ldw_disp16},
    {0xe0600000, 0xe1e00000, dec_mov_imm21},
    {0xedb00000, 0xfff0ffe0, dec_bld},
    {0xe0001700, 0xe1f0ff0f, dec_mov_cond_rdrs},
    {0xf9b00000, 0xfff0f000, dec_mov_cond_imm8},
    {0xe0001800, 0xe1f0ffff, dec_cpb},
    {0xe0001900, 0xe1f0ffff, dec_cph},
    {0xe0000d00, 0xe1f0fff0, dec_divu},
    {0xe0000c00, 0xe1f0fff0, dec_divs},
    {0xe0000300, 0xe1f0ffc0, dec_ldw_shift},
    {0xe0000400, 0xe1f0ffc0, dec_ldsh_shift},
    {0xe0000500, 0xe1f0ffc0, dec_lduh_shift},
    {0xe0000f80, 0xe1f0ffc0, dec_ldw_part},
    {0xe0000600, 0xe1f0ffc0, dec_ldsb_shift},
    {0xe0000700, 0xe1f0ffc0, dec_ldub_shift},
    {0xe0001100, 0xe1f0ff00, dec_rsub_imm8},
    {0xe1e02000, 0xe1f0fc00, dec_eor_shift},
    {0xe1d0b000, 0xe1f0fc00, dec_bfexts},
    {0xe1d0c000, 0xe1f0fc00, dec_bfextu},
    {0xe1d0d000, 0xe1f0fc00, dec_bfins},
    {0xe0001400, 0xe1f0ffe0, dec_asr_rs},
    {0xe0001500, 0xe1f0ffe0, dec_lsl_rs},
    {0xe0001600, 0xe1f0ffe0, dec_lsr_rs},
    {0xe0200000, 0xe1e00000, dec_sub_imm21},
    {0xe0000100, 0xe1f0ffc0, dec_sub_sa},
    {0xf5b00000, 0xfdf0f000, dec_subcond_imm},
    {0xe0400000, 0xe1e00000, dec_cpw_imm21},
    {0xe1e00000, 0xe1f0fc00, dec_and_shift},
    {0xe0000000, 0xe1f0ffc0, dec_add_sa},
    {0xe0000040, 0xe1f0fff0, dec_adc},
    {0xe0000140, 0xe1f0fff0, dec_sbc},
    {0xe0000240, 0xe1f0fff0, dec_mul_rxry},
    {0xe0000440, 0xe1f0fff0, dec_mulsd},
    {0xe0000640, 0xe1f0fff0, dec_mulud},
    {0xe0000340, 0xe1f0fff0, dec_mac},
    {0xe0000740, 0xe1f0fff0, dec_macud},
    {0xe0000c40, 0xe1f0fff0, dec_max},
    {0xe0000d40, 0xe1f0fff0, dec_min},
    {0xe0000840, 0xe1f0fff0, dec_asr_rxry},
    {0xe0000940, 0xe1f0fff0, dec_lsl_rxry},
    {0xe0000a40, 0xe1f0fff0, dec_lsr_rxry},
    {0xe0000a00, 0xe1f0ffc0, dec_sth_shift},
    {0xe0000b00, 0xe1f0ffc0, dec_stb_shift},
    {0xe0000900, 0xe1f0ffc0, dec_stw_shift},
    {0xe0000800, 0xe1f0ffc0, dec_std_shift},
    {0xe1600000, 0xe1f00000, dec_stb_disp16},
    {0xe0e00000, 0xe1f10000, dec_ldd_disp16},
    {0xe0e10000, 0xe1f10000, dec_std_disp16},
    {0xe0001300, 0xe1f0ffff, dec_cpc_rdrs},
    {0xe0001000, 0xe1f0ff00, dec_mul_rdrs},
    {0xf2100000, 0xfff00000, dec_pref},
    {0xe0001200, 0xe1f0ffff, dec_clz},
    {0xedc00000, 0xfdf00000, dec_stmts},
    {0xe5c00000, 0xfdf00000, dec_ldmts},
    {0xe0000b40, 0xe1f0fff0, dec_xchg},
    {0xf0100000, 0xfff00000, dec_mcall},

	{0, 0, dec_null}
};

static DecoderInfo decinfo_16[] = {
    {0xc008, 0xf00c, dec_rjmp},
    {0x3000, 0xf000, dec_mov_imm8},
    {0xd003, 0xfe0f, dec_csrfcz},
    {0x4800, 0xf800, dec_lddpc},
    {0xc000, 0xf008, dec_br_disp8},
    {0xa101, 0xe1f1, dec_ldd_pp},
    {0xa120, 0xe1f1, dec_std_pp},
    {0xa121, 0xe1f1, dec_std_mm},
    {0x0020, 0xe1f0, dec_rsub},
    {0xd001, 0xf00f, dec_pushm},
    {0xd002, 0xf007, dec_popm},
    {0x2000, 0xf000, dec_sub_imm8},
    {0xc00c, 0xf00c, dec_rcall_disp10},
    {0x0090, 0xe1f0, dec_mov_rdrs},
    {0x00a0, 0xe1f0, dec_stw_pp},
    {0x00d0, 0xe1f0, dec_stw_mm},
    {0x0100, 0xe1f0, dec_ldw_pp},
    {0x0140, 0xe1f0, dec_ldw_mm},
    {0x0110, 0xe1f0, dec_ldsh_pp},
    {0x0150, 0xe1f0, dec_ldsh_mm},
    {0x5c50, 0xffd0, dec_castu},
    {0x5cd0, 0xfff0, dec_com},
    {0x5c30, 0xfff0, dec_neg},
    {0x5c40, 0xfff0, dec_abs},
    {0x5e00, 0xff00, dec_ret},
    {0x5800, 0xfc00, dec_cpw_imm6},
    {0x8100, 0xe100, dec_stw_disp4},
    {0xa1a0, 0xe1e0, dec_sbr},
    {0xd403, 0xfe0f, dec_csrf},
    {0xd203, 0xfe0f, dec_ssrf},
    {0x6000, 0xe000, dec_ldw_disp5},
    {0x0130, 0xe1f0, dec_ldub_pp},
    {0x0170, 0xe1f0, dec_ldub_mm},
    {0x0030, 0xe1f0, dec_cpw},
    {0x0180, 0xe180, dec_ldub_disp3},
    {0x8000, 0xe180, dec_ldsh_disp3},
    {0x8080, 0xe180, dec_lduh_disp3},
    {0x5000, 0xf800, dec_stdsp},
    {0x4000, 0xf800, dec_lddsp},
    {0x00c0, 0xe1f0, dec_stb_pp},
    {0x00f0, 0xe1f0, dec_stb_mm},
    {0x00b0, 0xe1f0, dec_sth_pp},
    {0x00e0, 0xe1f0, dec_sth_mm},
    {0xa000, 0xe180, dec_sth_disp3},
    {0xa080, 0xe180, dec_stb_disp3},
    {0x0010, 0xe1f0, dec_sub_rdrs},
    {0x5f00, 0xff00, dec_sr},
    {0xa1c0, 0xe1e0, dec_cbr},
    {0x0000, 0xe1f0, dec_add},
    {0xa140, 0xe1e0, dec_asr},
    {0xa160, 0xe1e0, dec_lsl},
    {0xa180, 0xe1e0, dec_lsr},
    {0x0060, 0xe1f0, dec_and},
    {0x0080, 0xe1f0, dec_andn},
    {0x5d10, 0xfff0, dec_icall},
    {0x5c20, 0xfff0, dec_cpc},
    {0x0040, 0xe1f0, dec_or},
    {0x0050, 0xe1f0, dec_eor},
    {0x0070, 0xe1f0, dec_tst},
    {0xa130, 0xe1f0, dec_mul},
    {0x5ce0, 0xfff0, dec_tbnz},
    {0xd703, 0xffff, dec_nop},
    {0xd743, 0xffff, dec_frs},
    {0x5c90, 0xfff0, dec_brev},
    {0x5cb0, 0xfff0, dec_swap_b},
    {0x5d20, 0xfff0, dec_mustr},
    {0x5d00, 0xfff0, dec_ror},
    {0x5cf0, 0xfff0, dec_rol},
    {0x5c00, 0xfff0, dec_acr},
    {0xd603, 0xffff, dec_rete},
    {0xd613, 0xffff, dec_rets},
    {0xd733, 0xffff, dec_scall},
    {0xd643, 0xffff, dec_tlbr},
    {0xd653, 0xffff, dec_tlbs},
    {0xd663, 0xffff, dec_tlbw},

	{0, 0, dec_null}
};

static unsigned int
avr32_decode_32bit(DisasContext *dc)
{
    uint32_t insn_len;
    int i;

    dc->insn = ldl_code(dc->pc);
    dbg("32 bit insn: 0x%08x\n", dc->insn);

	for (i = 0; i < ARRAY_SIZE(decinfo_32); i++) {
		if ((dc->insn & decinfo_32[i].mask) == decinfo_32[i].bits)
		{
			insn_len = decinfo_32[i].dec(dc);
			break;
		}
	}

    return insn_len;
}

static unsigned int
avr32_decoder(DisasContext *dc)
{
	unsigned int insn_len = 2;
    int i;

    dbg("dc->pc = 0x%08x\n", dc->pc);

    dc->insn = lduw_code(dc->pc);
    if ((dc->insn & 0xe000) == 0xe000) {
        dc->insn = ldl_code(dc->pc);
        return avr32_decode_32bit(dc);
    }

    dbg("0x%08x\n", dc->insn);

	for (i = 0; i < ARRAY_SIZE(decinfo_16); i++) {
		if ((dc->insn & decinfo_16[i].mask) == decinfo_16[i].bits)
		{
			insn_len = decinfo_16[i].dec(dc);
			break;
		}
	}

    return insn_len;
}

static inline void gen_intermediate_code_internal(CPUState *env,
                                                  TranslationBlock *tb,
                                                  int search_pc)
{
	uint16_t *gen_opc_end;
	struct DisasContext ctx;
	struct DisasContext *dc = &ctx;
    uint32_t pc_start;
	int j, lj;
    unsigned int insn_len;
    int num_insns;
    int max_insns;
    uint32_t next_page_start;

    qemu_log_mask(CPU_LOG_TB_CPU, "------------------------------------------------\n");
    /* FIXME: This may print out stale hflags from env... */
    log_cpu_state_mask(CPU_LOG_TB_CPU, env, 0);

	gen_opc_end = gen_opc_buf + OPC_MAX_SIZE;

    pc_start = tb->pc;

    dc->env = env;
    dc->tb = tb;
    dc->is_jmp = DISAS_NEXT;
    dc->pc = tb->pc;

    dc->condjmp = 0;

    next_page_start = (pc_start & TARGET_PAGE_MASK) + TARGET_PAGE_SIZE;

#ifdef DEBUG
    dbg("%s search_pc =%d\n", __func__, search_pc);
    {
        int i;

        for (i = 0; i < 16; i++)
            dbg("env->regs[%d] = 0x%08x\n", i, env->regs[i]);
        dbg("env->sp_usr = 0x%08x\n", env->sp_usr);
        dbg("env->sp_sys = 0x%08x\n", env->sp_sys);
        dbg("env->sysreg[SYSREG_SR] = 0x%08x\n", env->sysreg[SYSREG_SR]);
    }
#endif

    lj = -1;
    num_insns = 0;
    max_insns = tb->cflags & CF_COUNT_MASK;
    if (max_insns == 0) {
        max_insns = CF_COUNT_MASK;
    }

    gen_icount_start();

    while (1) {
#ifdef DEBUG
        static int dumpinfo;
#endif

        if (search_pc) {
            j = gen_opc_ptr - gen_opc_buf;
            if (lj < j) {
                lj++;
                while (lj < j)
                    gen_opc_instr_start[lj++] = 0;
            }
            gen_opc_pc[lj] = dc->pc;
            gen_opc_instr_start[lj] = 1;
            gen_opc_icount[lj] = num_insns;
        }

        if (dc->condjmp && !dc->is_jmp) {
            gen_set_label(dc->condlabel);
            dc->condjmp = 0;
        }

        /* FIXME remove this and calculate pc in insns that need it */
        tcg_gen_movi_tl(cpu_R[15], dc->pc);

        insn_len = avr32_decoder(dc);

        dc->pc += insn_len;
        num_insns++;

#ifdef DEBUG
        if (dumpinfo) {
            unsigned int buf[9];
            unsigned int *intp = buf;
            unsigned int addr = 0x4007f508;
            int i;

            cpu_physical_memory_read(addr, (uint8_t *)buf, sizeof(buf));

            for (i = 0; i < sizeof(buf); i+=4) {
                dbg("0x%08x: 0x%08x\n", addr, *intp);
                addr += 4;
                intp++;
            }
        }
#endif

        if (dc->is_jmp) {
            break;
        }

        if (num_insns >= max_insns) {
            break;
        }

		if (gen_opc_ptr >= gen_opc_end) {
            break;
        }

        if (dc->pc >= next_page_start) {
            break;
        }
    }

    switch (dc->is_jmp) {
    case DISAS_NEXT:
        gen_goto_tb(dc, 1, dc->pc);
        break;

    case DISAS_JUMP:
        /* indicate that the hash table must be used to find the next TB */
        tcg_gen_exit_tb(0);
        break;
    }


    if (dc->condjmp) {
        gen_set_label(dc->condlabel);
        gen_goto_tb(dc, 1, dc->pc);
        dc->condjmp = 0;
    }

    // t0 = tcg_temp_new();
    // tcg_gen_andi_tl(t0, t0, 0x1f);

    // tcg_gen_goto_tb(0);
    // tcg_gen_exit_tb((long)tb);

    // t0 = tcg_temp_new();
    // tcg_gen_ori_tl(t0, t0, 0x1f);

    gen_icount_end(tb, num_insns);
    *gen_opc_ptr = INDEX_op_end;

    if (search_pc) {
        j = gen_opc_ptr - gen_opc_buf;
        lj++;
        while (lj <= j)
            gen_opc_instr_start[lj++] = 0;
    } else {
        tb->size = dc->pc - pc_start;
        tb->icount = num_insns;
    }
}

void gen_intermediate_code(CPUState *env, TranslationBlock *tb)
{
    gen_intermediate_code_internal(env, tb, 0);
}

void gen_intermediate_code_pc(CPUState *env, TranslationBlock *tb)
{
    gen_intermediate_code_internal(env, tb, 1);
}

void gen_pc_load(CPUState *env, TranslationBlock *tb,
                unsigned long searched_pc, int pc_pos, void *puc)
{
    env->regs[15] = gen_opc_pc[pc_pos];
}

void pic_info(Monitor *mon);
void irq_info(Monitor *mon);

/* Stub functions for hardware that doesn't exist.  */
void pic_info(Monitor *mon)
{
}

void irq_info(Monitor *mon)
{
}
