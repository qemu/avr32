/*
 *  AVR32 op helpers
 *
 *  Copyright (c) 2009 Rabin Vincent
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cpu.h"
#include "exec-all.h"
#include "gdbstub.h"
#include "qemu-common.h"

#ifdef DEBUG
#define dbg(format...) fprintf(stderr, format)
#else
static inline int dbg(const char *format, ...)
{
    return 0;
}
#endif

target_phys_addr_t cpu_get_phys_page_debug(CPUState *env, target_ulong addr)
{
    return addr;
}

void cpu_reset(CPUAVR32State *env)
{
    env->regs[15] = 0;
    env->sysreg[SYSREG_SR] = SR_M_SUP | SR_EM | SR_GM;
    env->sysreg[SYSREG_CONFIG0] = 0x01012510;
    env->sysreg[SYSREG_CONFIG1] = 0x00118020;
    env->sysreg[SYSREG_MMUCR] = MMUCR_SEGMENT_EN;
    env->sysreg[SYSREG_TLBARLO] = ~0u;
    env->sysreg[SYSREG_TLBARHI] = ~0u;

    tlb_flush(env, 1);
}

void do_interrupt(CPUState *env)
{
    uint32_t offset;
    uint32_t mask;
    uint32_t value;
    int rar;
    int rsr;

    dbg("$$ %s [0x%#x]\n", __func__, env->sysreg[SYSREG_SR]);
    dbg("%s, exception_index=%d\n", __func__, env->exception_index);

    /* FIXME handled banked registers */

    switch (env->exception_index) {
    case EXCP_ILLEGAL:
        offset = 0x20;

        mask = SR_M | SR_J | SR_R | SR_EM | SR_GM;
        value = SR_M_EXP | SR_EM | SR_GM;
        rar = SYSREG_RAR_EX;
        rsr = SYSREG_RSR_EX;
        break;

    case EXCP_DTLB_WRITE_PROTECT:
        offset = 0x40;

        mask = SR_M | SR_J | SR_R | SR_EM | SR_GM;
        value = SR_M_EXP | SR_EM | SR_GM;
        rar = SYSREG_RAR_EX;
        rsr = SYSREG_RSR_EX;
        break;

    case EXCP_DTLB_READ_MISS:
        offset = 0x60;

        mask = SR_M | SR_J | SR_R | SR_EM | SR_GM;
        value = SR_M_EXP | SR_EM | SR_GM;
        rar = SYSREG_RAR_EX;
        rsr = SYSREG_RSR_EX;
        break;

    case EXCP_DTLB_WRITE_MISS:
        offset = 0x70;

        mask = SR_M | SR_J | SR_R | SR_EM | SR_GM;
        value = SR_M_EXP | SR_EM | SR_GM;
        rar = SYSREG_RAR_EX;
        rsr = SYSREG_RSR_EX;
        break;

    case EXCP_ITLB_MISS:
        offset = 0x50;

        mask = SR_M | SR_J | SR_R | SR_EM | SR_GM;
        value = SR_M_EXP | SR_EM | SR_GM;
        rar = SYSREG_RAR_EX;
        rsr = SYSREG_RSR_EX;
        break;

    case EXCP_INT0:
        offset = env->autovector[0];
        mask = SR_M | SR_J | SR_R | SR_I0M;
        value = SR_M_INT0 | SR_I0M;

        rar = SYSREG_RAR_INT0;
        rsr = SYSREG_RSR_INT0;
        break;

    default:
        cpu_abort(env, "Unhandled exception 0x%x\n", env->exception_index);
    }

    env->sysreg[rsr] = env->sysreg[SYSREG_SR];
    env->sysreg[rar] = env->regs[15];
    env->sysreg[SYSREG_ECR] = offset >> 2;

    if ((env->sysreg[SYSREG_SR] & SR_M) == SR_M_APP) {
        env->sp_usr = env->regs[13];
        env->regs[13] = env->sp_sys;
    }


    dbg("%s: offset: %x\n", __func__, offset);
    dbg("%s: rar: %x\n", __func__, env->sysreg[rar]);
    dbg("%s: SYSREG_RAR_EX: %x\n", __func__, env->sysreg[SYSREG_RAR_EX]);

    env->sysreg[SYSREG_SR] = (env->sysreg[SYSREG_SR] &~ mask) | value;
    env->regs[15] = env->sysreg[SYSREG_EVBA] + offset;

    env->exception_index = 0;

    {
        int i;

        for (i = 0; i < 16; i++)
            dbg("env->regs[%d] = 0x%08x\n", i, env->regs[i]);
        dbg("env->sp_usr = 0x%08x\n", env->sp_usr);
        dbg("env->sp_sys = 0x%08x\n", env->sp_sys);
        dbg("env->sysreg[SYSREG_SR] = 0x%08x\n", env->sysreg[SYSREG_SR]);
    }
}

