/*
 *  AVR32 helper routines
 *
 *  Copyright (c) 2009 Rabin Vincent
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston MA  02110-1301 USA
 */

#include "exec.h"
#include "helper.h"
#include "host-utils.h"

#include "cpu.h"
#include "exec-all.h"
#include "disas.h"
#include "tcg-op.h"
#include "qemu-common.h"

#ifdef DEBUG
#define dbg(format...) fprintf(stderr, format)
#else
static inline int dbg(const char *format, ...)
{
    return 0;
}
#endif

void raise_exception(int tt)
{
    env->exception_index = tt;
    cpu_loop_exit();
}


#if !defined(CONFIG_USER_ONLY)

#define MMUSUFFIX _mmu

#define SHIFT 0
#include "softmmu_template.h"

#define SHIFT 1
#include "softmmu_template.h"

#define SHIFT 2
#include "softmmu_template.h"

#define SHIFT 3
#include "softmmu_template.h"

/* try to fill the TLB and return an exception if error. If retaddr is
   NULL, it means that the function was called in C code (i.e. not
   from generated code or from helper.c) */
/* XXX: fix it to restore all registers */
void tlb_fill (target_ulong addr, int is_write, int mmu_idx, void *retaddr)
{
    TranslationBlock *tb;
    CPUState *saved_env;
    unsigned long pc;
    int ret;

    // printf("%s addr=0x%08x\n", __func__, addr);

    /* XXX: hack to restore env in all cases, even if not called from
       generated code */
    saved_env = env;
    env = cpu_single_env;
    ret = cpu_avr32_handle_mmu_fault(env, addr, is_write, mmu_idx, 1);
    if (unlikely(ret)) {
        if (retaddr) {
            dbg("faild in tlb_fill\n");
            /* now we have a real cpu fault */
            pc = (unsigned long)retaddr;
            tb = tb_find_pc(pc);
            if (tb) {
                /* the PC is inside the translated code. It means that we have
                   a virtual CPU fault */
                cpu_restore_state(tb, env, pc, NULL);
            }
        }
        raise_exception(env->exception_index);
    }
    env = saved_env;
}
#endif

void HELPER(mmucr_write)(uint32_t value)
{
    int i;

    env->sysreg[SYSREG_MMUCR] = value & ~MMUCR_INVALIDATE;

    if (value & MMUCR_INVALIDATE) {
        for (i = 0; i < ARRAY_SIZE(env->tlb); i++) {
            AVR32Tlb *tlb = &env->tlb[i];
            tlb->tlbehi = 0;
        }

        tlb_flush(env, 1);
    }
}

void HELPER(tlb_flush)(void)
{
   tlb_flush(env, 1);
}

void HELPER(debug)(void)
{
#ifdef DEBUG
	extern void debug_dump(CPUState *env);
	extern int dumping;

	if (env->regs[15] == 0x0001d96a)
		dumping = 1;

	switch (env->regs[15]) {
	case 0x0001d96a:
		if (dumping) {
			printf("%s pc=%#x\n", __func__, env->regs[15]);
			debug_dump(env);
		}
	}
#endif
}

void HELPER(tbnz)(uint32_t value)
{
    if (!(value & 0xff) || !(value & 0xff00) || !(value & 0xff0000)
            || !(value & 0xff000000)) {
        env->sysreg[SYSREG_SR] |= SR_Z;
    } else {
        env->sysreg[SYSREG_SR] &= ~SR_Z;
    }
}

void HELPER(csrfcz)(uint32_t bit)
{
    uint32_t sysreg = env->sysreg[SYSREG_SR];
    uint32_t value = !!(sysreg & bit);

    if (value) {
        sysreg |= SR_C | SR_Z;
    } else {
        sysreg = sysreg & ~(SR_C | SR_Z);
    }

    env->sysreg[SYSREG_SR] = sysreg;
}

uint32_t HELPER(ror)(uint32_t reg)
{
    uint32_t sysreg = env->sysreg[SYSREG_SR];
    uint32_t mask = SR_N | SR_Z | SR_C;
    uint32_t value = 0;

    if (reg & 1) {
        value |= SR_C;
    }

    reg >>= 1;
    reg |= (sysreg & SR_C) << 31;

    if (reg & (1 << 31)) {
        value |= SR_N;
    }

    if (!reg) {
        value |= SR_Z;
    }

    env->sysreg[SYSREG_SR] = (sysreg & ~mask) | value;

    return reg;
}

uint32_t HELPER(rol)(uint32_t reg)
{
    uint32_t sysreg = env->sysreg[SYSREG_SR];
    uint32_t mask = SR_N | SR_Z | SR_C;
    uint32_t value = 0;

    if (reg & (1 << 31)) {
        value |= SR_C;
    }

    reg <<= 1;
    reg |= (sysreg & SR_C);

    if (reg & (1 << 31)) {
        value |= SR_N;
    }

    if (!reg) {
        value |= SR_Z;
    }

    env->sysreg[SYSREG_SR] = (sysreg & ~mask) | value;

    return reg;
}

uint32_t HELPER(acr)(uint32_t reg)
{
    uint32_t sysreg = env->sysreg[SYSREG_SR];
    uint32_t mask = SR_V | SR_N | SR_Z | SR_C;
    uint32_t value = 0;
    uint32_t orig = reg;

    if (sysreg & SR_C) {
        reg += 1;
    }

    if ((reg & ~orig) & (1 << 31)) {
        value |= SR_V;
    }

    if ((~reg & orig) & (1 << 31)) {
        value |= SR_C;
    }

    if (reg & (1 << 31)) {
        value |= SR_N;
    }

    if (!reg && (sysreg & SR_Z)) {
        value |= SR_Z;
    }

    env->sysreg[SYSREG_SR] = (sysreg & ~mask) | value;

    return reg;
}

void HELPER(cc_and)(uint32_t res)
{
    uint32_t mask = SR_N | SR_Z;
    uint32_t value;

    value = (res >> 29) & SR_N;

    if (!res) {
        value |= SR_Z;
    }

    env->sysreg[SYSREG_SR] = (env->sysreg[SYSREG_SR] & ~mask) | value;
}

void HELPER(cc_neg)(uint32_t res, uint32_t orig)
{
    uint32_t mask = SR_V | SR_N | SR_Z | SR_C;
    uint32_t value;

    value = (res >> 29) & SR_N;

    if (!res) {
        value |= SR_Z;
    }

    if ((orig & res) & (1 << 31)) {
        value |= SR_V;
    }

    if ((orig | res) & (1 << 31)) {
        value |= SR_C;
    }

    env->sysreg[SYSREG_SR] = (env->sysreg[SYSREG_SR] & ~mask) | value;
}

void HELPER(cc_com)(uint32_t res)
{
    uint32_t mask = SR_Z;
    uint32_t value = 0;

    if (!res) {
        value |= SR_Z;
    }

    env->sysreg[SYSREG_SR] = (env->sysreg[SYSREG_SR] & ~mask) | value;
}

void HELPER(cc_castu)(uint32_t res)
{
    uint32_t mask = SR_N | SR_Z | SR_C;
    uint32_t value = 0;

    if (!res) {
        value |= SR_Z;
    }

    env->sysreg[SYSREG_SR] = (env->sysreg[SYSREG_SR] & ~mask) | value;
}

void HELPER(cc_add)(uint32_t res, uint32_t op1, uint32_t op2)
{
    uint32_t mask = SR_V | SR_N | SR_Z | SR_C;
    uint32_t value;

    value = (res >> 29) & SR_N;

    if (!res) {
        value |= SR_Z;
    }

    if (((op1 & op2 & ~res) | (~op1 & ~op2 & res)) & (1 << 31)) {
        value |= SR_V;
    }

    if (((op1 & op2) | (op1 & ~res) | (op2 & ~res)) & (1 << 31)) {
        value |= SR_C;
    }

    env->sysreg[SYSREG_SR] = (env->sysreg[SYSREG_SR] & ~mask) | value;
}

void HELPER(cc_cpc)(uint32_t res, uint32_t op1, uint32_t op2)
{
    uint32_t mask = SR_V | SR_N | SR_Z | SR_C;
    uint32_t value;

    value = (res >> 29) & SR_N;

    if (!res && (env->sysreg[SYSREG_SR] & SR_Z)) {
        value |= SR_Z;
    }

    if (((op1 & ~op2 & ~res) | (~op1 & op2 & res)) & (1 << 31)) {
        value |= SR_V;
    }

    if (((~op1 & op2) | (op2 & res) | (~op1 & res)) & (1 << 31)) {
        value |= SR_C;
    }

    env->sysreg[SYSREG_SR] = (env->sysreg[SYSREG_SR] & ~mask) | value;
}

static void cc_sub(uint32_t res, uint32_t op1, uint32_t op2, int bits)
{
    uint32_t mask = SR_V | SR_N | SR_Z | SR_C;
    uint32_t value;

    value = (res >> (bits - 3)) & SR_N;

    if (!res) {
        value |= SR_Z;
    }

    if (((op1 & ~op2 & ~res) | (~op1 & op2 & res)) & (1 << (bits - 1))) {
        value |= SR_V;
    }

    if (((~op1 & op2) | (op2 & res) | (~op1 & res)) & (1 << (bits - 1))) {
        value |= SR_C;
    }

    env->sysreg[SYSREG_SR] = (env->sysreg[SYSREG_SR] & ~mask) | value;
}

void HELPER(cc_sub)(uint32_t res, uint32_t op1, uint32_t op2)
{
    cc_sub(res, op1, op2, 32);
}

void HELPER(cc_subb)(uint32_t res, uint32_t op1, uint32_t op2)
{
    cc_sub(res, op1, op2, 8);
}

void HELPER(cc_subh)(uint32_t res, uint32_t op1, uint32_t op2)
{
    cc_sub(res, op1, op2, 16);
}

void HELPER(cc_ret)(uint32_t res)
{
    uint32_t mask = SR_N | SR_Z | SR_V | SR_C;
    uint32_t value;

    value = (res >> 29) & SR_N;

    if (!res) {
        value |= SR_Z;
    }

    env->sysreg[SYSREG_SR] = (env->sysreg[SYSREG_SR] & ~mask) | value;
}

void HELPER(cc_bfextu)(uint32_t res)
{
    uint32_t mask = SR_N | SR_Z | SR_C;
    uint32_t value = 0;

    if (res & (1 << 31)) {
        value |= SR_N | SR_C;
    }

    if (!res) {
        value |= SR_Z;
    }

    env->sysreg[SYSREG_SR] = (env->sysreg[SYSREG_SR] & ~mask) | value;
}

void HELPER(cc_lsr)(uint32_t res, uint32_t op, uint32_t shamt)
{
    uint32_t mask = SR_N | SR_Z | SR_C;
    uint32_t value;

    value = (res >> 29) & SR_N;

    if (shamt && (op & (1 << (shamt - 1)))) {
        value |= SR_C;
    }

    if (!res) {
        value |= SR_Z;
    }

    env->sysreg[SYSREG_SR] = (env->sysreg[SYSREG_SR] & ~mask) | value;
}

void HELPER(cc_lsl)(uint32_t res, uint32_t op, uint32_t shamt)
{
    uint32_t mask = SR_N | SR_Z | SR_C;
    uint32_t value;

    value = (res >> 29) & SR_N;

    if (shamt && (op & (1 << (32 - shamt)))) {
        value |= SR_C;
    }

    if (!res) {
        value |= SR_Z;
    }

    env->sysreg[SYSREG_SR] = (env->sysreg[SYSREG_SR] & ~mask) | value;
}

uint32_t HELPER(brev)(uint32_t orig)
{
    uint32_t mask = SR_N | SR_Z;
    uint32_t value = 0;
    uint32_t res = 0;
    int i;

    for (i = 0; orig; i++, orig >>= 1) {
        res |= (orig & 0x1) << (31 - i);
    }

    if (!res) {
        value |= SR_Z;
    }

    env->sysreg[SYSREG_SR] = (env->sysreg[SYSREG_SR] & ~mask) | value;

    return res;
}

uint32_t HELPER(clz)(uint32_t rs)
{
    uint32_t mask = SR_Z | SR_C;
    uint32_t value = 0;
    int count;

    for (count = 32; rs; count--) {
        rs >>= 1;
    }

    if (!count) {
        value |= SR_Z;
    } else if (count == 32) {
        value |= SR_C;
    }

    env->sysreg[SYSREG_SR] = (env->sysreg[SYSREG_SR] & ~mask) | value;

    return count;
}

void HELPER(raise_exception)(uint32_t index)
{
    dbg("$$ %s [0x%#x]\n", __func__, env->sysreg[SYSREG_SR]);

    env->exception_index = index;
    cpu_loop_exit();
}

void HELPER(scall)(void)
{
    dbg("$$ %s [0x%#x]\n", __func__, env->sysreg[SYSREG_SR]);

#ifdef CONFIG_USER_ONLY
    raise_exception(EXCP_SYSCALL);
#else
    switch (env->sysreg[SYSREG_SR] & SR_M) {
    case SR_M_APP:
        env->sp_usr = env->regs[13];
        env->regs[13] = env->sp_sys;

        /* fallthru */

    case SR_M_SUP:
        env->sysreg[SYSREG_RAR_SUP] = env->regs[15] + 2;
        env->sysreg[SYSREG_RSR_SUP] = env->sysreg[SYSREG_SR];

        env->sysreg[SYSREG_SR] |= SR_M_SUP;
        break;

    default:
        env->regs[14] = env->regs[15] + 2;
        break;
    }

    env->regs[15] = env->sysreg[SYSREG_EVBA] + 0x100;
#endif
}

void HELPER(rets)(void)
{
    dbg("$$ %s [0x%#x]\n", __func__, env->sysreg[SYSREG_SR]);

    switch (env->sysreg[SYSREG_SR] & SR_M) {
    case SR_M_APP:
        /* FIXME privilege violation */
        break;

    case SR_M_SUP:
        env->sysreg[SYSREG_SR] = env->sysreg[SYSREG_RSR_SUP];
        env->regs[15] = env->sysreg[SYSREG_RAR_SUP];

        if (!env->regs[15]) {
            printf("%s: NULL pointer derefernce in rets\n", __func__);
            cpu_abort(env, "fail\n");
        }

        if ((env->sysreg[SYSREG_SR] & SR_M) == SR_M_APP) {
            env->sp_sys = env->regs[13];
            env->regs[13] = env->sp_usr;
        }
        break;

    default:
        env->regs[15] = env->regs[14];
        break;
    }
}

void HELPER(rete)(void)
{
    int sr;
    int pc;

    dbg("$$ %s [0x%#x]\n", __func__, env->sysreg[SYSREG_SR]);

    switch (env->sysreg[SYSREG_SR] & SR_M) {
    case SR_M_EXP:
        sr = SYSREG_RSR_EX;
        pc = SYSREG_RAR_EX;
        break;

    case SR_M_INT0:
        sr = SYSREG_RSR_INT0;
        pc = SYSREG_RAR_INT0;
        break;

    default:
        cpu_abort(env, "Unknown state in rete: %x\n", env->sysreg[SYSREG_SR]);
    }

    dbg("%s: SYSREG_RAR_EX:%x\n", __func__, env->sysreg[SYSREG_RAR_EX]);
    dbg("%s: saved pc:%x\n", __func__, env->sysreg[pc]);

    env->sysreg[SYSREG_SR] = env->sysreg[sr] &~ SR_L;
    env->regs[15] = env->sysreg[pc];

    if ((env->sysreg[SYSREG_SR] & SR_M) == SR_M_APP) {
        dbg("returning to APP mode\n");
        env->sp_sys = env->regs[13];
        env->regs[13] = env->sp_usr;
    }

#if 0
    {
        int i;

        for (i = 0; i < 16; i++)
            fprintf(stderr, "env->regs[%d] = 0x%08x\n", i, env->regs[i]);
        fprintf(stderr, "env->sp_usr = 0x%08x\n", env->sp_usr);
        fprintf(stderr, "env->sp_sys = 0x%08x\n", env->sp_sys);
        fprintf(stderr, "env->sysreg[SYSREG_SR] = 0x%08x\n", env->sysreg[SYSREG_SR]);
    }
#endif

    /* FIXME handle rete from INT*, restore banked regs */
}

uint32_t HELPER(read_counter)(void)
{
#ifndef CONFIG_USER_ONLY
    return avr32_read_counter();
#else
    return 0;
#endif
}

void HELPER(write_compare)(uint32_t value)
{
    env->sysreg[SYSREG_COMPARE] = value;
#ifndef CONFIG_USER_ONLY
    avr32_write_compare(value);
#endif
}

static void dump_tlb(void)
{
    int i;
    uint32_t tlbehi = env->sysreg[SYSREG_TLBEHI];
    uint32_t tlbelo = env->sysreg[SYSREG_TLBELO];
    uint32_t mmucr = env->sysreg[SYSREG_MMUCR];

    for (i = 0; i < ARRAY_SIZE(env->tlb); i++) {
        AVR32Tlb *tlb = &env->tlb[i];
        int valid = (((tlb->tlbehi & ~0x3ff) == (tlbehi & ~0x3ff)) && (tlb->tlbelo & 0xc) == (tlbelo & 0xc) && (tlb->tlbehi & TLBEHI_VALID));
        int exclude = (!(mmucr & MMUCR_MODE_SHARED) && !(tlb->tlbelo & TLBELO_G) && ((tlb->tlbehi & 0xff) != (tlbehi & 0xff)));

        dbg("index=%d tlbehi=0x%x tlbelo=0x%x valid=%d exclude=%d\n", i, tlb->tlbehi, tlb->tlbelo, valid, exclude);
    }
}

void HELPER(tlbs)(void)
{
    uint32_t tlbehi = env->sysreg[SYSREG_TLBEHI];
    uint32_t tlbelo = env->sysreg[SYSREG_TLBELO];
    uint32_t mmucr = env->sysreg[SYSREG_MMUCR];
    int i;

    for (i = 0; i < ARRAY_SIZE(env->tlb); i++) {
        AVR32Tlb *tlb = &env->tlb[i];

        if (((tlb->tlbehi & ~0x3ff) == (tlbehi & ~0x3ff)) &&
                (tlb->tlbelo & 0xc) == (tlbelo & 0xc) &&
                (tlb->tlbehi & TLBEHI_VALID)) {

            if (!(mmucr & MMUCR_MODE_SHARED) &&
                !(tlb->tlbelo & TLBELO_G) &&
                ((tlb->tlbehi & 0xff) != (tlbehi & 0xff)))
            {
                continue;
            }

            break;
        }
    }

    if (i < ARRAY_SIZE(env->tlb)) {
        mmucr &= ~(MMUCR_NOT_FOUND | (0x3f << MMUCR_DRP_SHIFT));
        mmucr |= i << MMUCR_DRP_SHIFT;
        dbg("TLB found tlbehi=0x%08x tlbelo=0x%08x i=%d\n", tlbehi, tlbelo, i);
    } else {
        dbg("TLB not found tlbehi=0x%08x tlbelo=0x%08x\n", tlbehi, tlbelo);
        dump_tlb();
        mmucr |= MMUCR_NOT_FOUND;
    }

    env->sysreg[SYSREG_MMUCR] = mmucr;
}

void HELPER(tlbw)(void)
{
    uint32_t tlbehi = env->sysreg[SYSREG_TLBEHI];
    uint32_t tlbelo = env->sysreg[SYSREG_TLBELO];
    uint32_t mmucr = env->sysreg[SYSREG_MMUCR];
    int dpr = (mmucr >> MMUCR_DRP_SHIFT) & 0x3f;
    AVR32Tlb *tlb = &env->tlb[dpr];

    dbg("%s tlbehi: %x, tlbelo: %x dpr: %d\n", __func__, tlbehi, tlbelo, dpr);

    tlb->tlbehi = tlbehi;
    tlb->tlbelo = tlbelo;
    tlb_flush(env, 1);
}

void HELPER(tlbr)(void)
{
    uint32_t mmucr = env->sysreg[SYSREG_MMUCR];
    int dpr = (mmucr >> MMUCR_DRP_SHIFT) & 0x3f;
    AVR32Tlb *tlb = &env->tlb[dpr];

    env->sysreg[SYSREG_TLBEHI] = tlb->tlbehi;
    env->sysreg[SYSREG_TLBELO] = tlb->tlbelo;
    tlb_flush(env, 1);
}
