#include "def-helper.h"

DEF_HELPER_0(debug, void);
DEF_HELPER_1(mmucr_write, void, i32);
DEF_HELPER_0(tlb_flush, void);

DEF_HELPER_0(rete, void);
DEF_HELPER_0(rets, void);
DEF_HELPER_0(scall, void);
DEF_HELPER_1(raise_exception, void, i32);

DEF_HELPER_0(tlbs, void);
DEF_HELPER_0(tlbw, void);
DEF_HELPER_0(tlbr, void);

DEF_HELPER_0(read_counter, i32);
DEF_HELPER_1(write_compare, void, i32);

DEF_HELPER_1(csrfcz, void, i32);
DEF_HELPER_1(tbnz, void, i32);
DEF_HELPER_1(cc_and, void, i32);
DEF_HELPER_1(cc_castu, void, i32);
DEF_HELPER_1(cc_com, void, i32);
DEF_HELPER_2(cc_neg, void, i32, i32);
DEF_HELPER_1(cc_ret, void, i32);
DEF_HELPER_1(cc_bfextu, void, i32);
DEF_HELPER_3(cc_add, void, i32, i32, i32);
DEF_HELPER_3(cc_cpc, void, i32, i32, i32);
DEF_HELPER_3(cc_sub, void, i32, i32, i32);
DEF_HELPER_3(cc_subb, void, i32, i32, i32);
DEF_HELPER_3(cc_subh, void, i32, i32, i32);
DEF_HELPER_3(cc_lsr, void, i32, i32, i32);
DEF_HELPER_3(cc_lsl, void, i32, i32, i32);

DEF_HELPER_1(brev, i32, i32)
DEF_HELPER_1(clz, i32, i32)
DEF_HELPER_1(ror, i32, i32);
DEF_HELPER_1(rol, i32, i32);
DEF_HELPER_1(acr, i32, i32);

#include "def-helper.h"
